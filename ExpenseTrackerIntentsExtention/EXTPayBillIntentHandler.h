//
//  EXTPayBillIntentHandler.h
//  ExpenseTrackerIntentsExtention
//
//  Created by sbt-klochkov-dv on 12/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Intents/Intents.h>

@interface EXTPayBillIntentHandler : NSObject<INPayBillIntentHandling>

@end
