//
//  EXTSendPaymentIntentHandler.h
//  ExpenseTrackerIntentsExtention
//
//  Created by sbt-klochkov-dv on 13/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Intents/Intents.h>
#import "EXTExpenseServiceProtocol.h"

@interface EXTSendPaymentIntentHandler : NSObject<INSendPaymentIntentHandling>

@property(nonatomic, strong) id<EXTExpenseServiceProtocol> expenseService;

@end
