//
//  EXTTransferMoneyIntentHandler.m
//  ExpenseTrackerIntentsExtention
//
//  Created by sbt-klochkov-dv on 09/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTTransferMoneyIntentHandler.h"

@implementation EXTTransferMoneyIntentHandler


- (void)handleTransferMoney:(nonnull INTransferMoneyIntent *)intent completion:(nonnull void (^)(INTransferMoneyIntentResponse * _Nonnull))completion {
    
    if (intent.toAccount) {
        
        NSString * accountName = [self getNameFromAccount:intent.toAccount];
        id<EXTExpenseCategoryProtocol> category = [_expenseService findCategoryByName:accountName];
        
        if (!category) {
            category = [_expenseService createNewCategoryWithName:accountName];
        }
        
        NSDecimalNumber * amount = intent.transactionAmount.amount.amount;
        
        id<EXTExpenseProtocol> expense = [_expenseService createNewExpense];
        
        expense.amount = amount;
        expense.note = intent.transactionNote;
        expense.category = category;
        
        [_expenseService saveExpense:expense];
        
        INTransferMoneyIntentResponse * response = [[INTransferMoneyIntentResponse alloc] initWithCode:INTransferMoneyIntentResponseCodeSuccess userActivity:nil];
        
        response.transactionAmount = intent.transactionAmount;
        response.toAccount = intent.toAccount;
        response.transactionNote = intent.transactionNote;
        
        
        completion(response);
        
    }
}

-(void)confirmTransferMoney:(INTransferMoneyIntent *)intent completion:(void (^)(INTransferMoneyIntentResponse * _Nonnull))completion {
    if ([intent transactionAmount] == nil || [intent toAccount] == nil) {
        completion([[INTransferMoneyIntentResponse alloc] initWithCode:(INTransferMoneyIntentResponseCodeFailure) userActivity:nil]);
    } else {
        INTransferMoneyIntentResponse * response = [[INTransferMoneyIntentResponse alloc] initWithCode:(INTransferMoneyIntentResponseCodeReady) userActivity:nil];
        response.transactionAmount = intent.transactionAmount;
        response.toAccount = intent.toAccount;
        response.transactionNote = intent.transactionNote;
        completion(response);
    }
}

- (void)resolveTransactionNoteForTransferMoney:(INTransferMoneyIntent *)intent withCompletion:(void (^)(INStringResolutionResult * _Nonnull))completion {
    
    if ([intent transactionNote] == nil) {
        completion([INStringResolutionResult notRequired]);
    } else {
        completion([INStringResolutionResult successWithResolvedString:[intent transactionNote]]);
    }
    
}


-(void)resolveToAccountForTransferMoney:(INTransferMoneyIntent *)intent withCompletion:(void (^)(INPaymentAccountResolutionResult * _Nonnull))completion {
    if (!intent.toAccount) {
        completion([INPaymentAccountResolutionResult needsValue]);
        return;
    } else {
        
        NSString * accountName = [self getNameFromAccount:intent.toAccount];
        
        if (!accountName) {
            completion([INPaymentAccountResolutionResult needsValue]);
            return;
        }
        
        id<EXTExpenseCategoryProtocol> category = [_expenseService findCategoryByName:accountName];
        
        if (!category) {
            completion([INPaymentAccountResolutionResult confirmationRequiredWithPaymentAccountToConfirm:intent.toAccount]);
            return;
        }
        completion([INPaymentAccountResolutionResult successWithResolvedPaymentAccount:intent.toAccount]);
    }
}


-(NSString *) getNameFromAccount:(INPaymentAccount *) account {
    NSString * accountName = nil;
    
    if (account.organizationName) {
        accountName = [account.organizationName.spokenPhrase capitalizedString];
    } else if(account.nickname) {
        accountName = [account.nickname.spokenPhrase capitalizedString];
    }
    
    return accountName;
}





@end
