//
//  EXTPayBillIntentHandler.m
//  ExpenseTrackerIntentsExtention
//
//  Created by sbt-klochkov-dv on 12/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTPayBillIntentHandler.h"

@implementation EXTPayBillIntentHandler




-(void)handlePayBill:(INPayBillIntent *)intent completion:(void (^)(INPayBillIntentResponse * _Nonnull))completion {
    NSLog(@"Handle intent : %@", intent);
    
    
}

- (void)resolveBillPayeeForPayBill:(INPayBillIntent *)intent withCompletion:(void (^)(INBillPayeeResolutionResult * _Nonnull))completion {
    NSLog(@"payee : %@", intent.billPayee);
}

@end
