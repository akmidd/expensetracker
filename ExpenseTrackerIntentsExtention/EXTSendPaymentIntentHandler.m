//
//  EXTSendPaymentIntentHandler.m
//  ExpenseTrackerIntentsExtention
//
//  Created by sbt-klochkov-dv on 13/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTSendPaymentIntentHandler.h"
#import "EXTUtils.h"

@implementation EXTSendPaymentIntentHandler


- (void)handleSendPayment:(INSendPaymentIntent *)intent completion:(void (^)(INSendPaymentIntentResponse * _Nonnull))completion {
    
    
    id<EXTExpenseCategoryProtocol> category = [_expenseService findCategoryByName:intent.note];
    
    if (!category) {
        category = [_expenseService createNewCategoryWithName:intent.note];
    }
    
    NSDecimalNumber * amount = intent.currencyAmount.amount;
    
    id<EXTExpenseProtocol> expense = [_expenseService createNewExpense];
    
    expense.amount = amount;
    
    expense.category = category;
    
    [_expenseService saveExpense:expense];
    
    INSendPaymentIntentResponse * response = [[INSendPaymentIntentResponse alloc] initWithCode:INSendPaymentIntentResponseCodeSuccess userActivity:nil];
    
    INPaymentRecord * paymentRecord = [[INPaymentRecord alloc] initWithPayee:intent.payee payer:nil currencyAmount:intent.currencyAmount paymentMethod:nil note:intent.note status:(INPaymentStatusUnknown) feeAmount:nil];
    
    response.paymentRecord = paymentRecord;
    
    
    completion(response);
    
}

- (void)resolvePayeeForSendPayment:(INSendPaymentIntent *)intent completion:(void (^)(INSendPaymentPayeeResolutionResult * _Nonnull))completion {
    
    INPerson * fakePerson = [[INPerson alloc] initWithPersonHandle:[[INPersonHandle alloc] initWithValue:@"" type:INPersonHandleTypeUnknown] nameComponents:nil displayName:nil image:nil contactIdentifier:nil customIdentifier:nil];
    completion([INSendPaymentPayeeResolutionResult successWithResolvedPerson:fakePerson]);
   
}

-(void)confirmSendPayment:(INSendPaymentIntent *)intent completion:(void (^)(INSendPaymentIntentResponse * _Nonnull))completion {
    
    INSendPaymentIntentResponse * response = [[INSendPaymentIntentResponse alloc] initWithCode:(INSendPaymentIntentResponseCodeSuccess) userActivity:nil];
    
    INPaymentRecord * paymentRecord = [[INPaymentRecord alloc] initWithPayee:intent.payee payer:nil currencyAmount:intent.currencyAmount paymentMethod:nil note:intent.note status:(INPaymentStatusUnknown) feeAmount:nil];
    
    response.paymentRecord = paymentRecord;
    
    completion(response);
    
}

-(void)resolveNoteForSendPayment:(INSendPaymentIntent *)intent withCompletion:(void (^)(INStringResolutionResult * _Nonnull))completion {
    
    if ([intent note] == nil) {
        completion([INStringResolutionResult needsValue]);
    } else {
        NSString * note = [EXTUtils getFirstNounFromString:[intent note]];
        
        completion([INStringResolutionResult successWithResolvedString:[note capitalizedString]]);
    }
    
    
    
}




@end
