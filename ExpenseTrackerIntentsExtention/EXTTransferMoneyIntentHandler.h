//
//  EXTTransferMoneyIntentHandler.h
//  ExpenseTrackerIntentsExtention
//
//  Created by sbt-klochkov-dv on 09/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Intents/Intents.h>
#import "EXTExpenseServiceProtocol.h"

@interface EXTTransferMoneyIntentHandler : NSObject<INTransferMoneyIntentHandling>

@property(nonatomic, strong) id<EXTExpenseServiceProtocol> expenseService;

@end
