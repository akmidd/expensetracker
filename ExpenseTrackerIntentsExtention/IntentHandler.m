//
//  IntentHandler.m
//  ExpenseTrackerIntentsExtention
//
//  Created by sbt-klochkov-dv on 09/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "IntentHandler.h"
#import "EXTTransferMoneyIntentHandler.h"
#import "EXTPayBillIntentHandler.h"
#import "EXTSendPaymentIntentHandler.h"
#import "EXTExpenseService.h"

// As an example, this class is set up to handle Message intents.
// You will want to replace this or add other intents as appropriate.
// The intents you wish to handle must be declared in the extension's Info.plist.

// You can test your example integration by saying things to Siri like:
// "Send a message using <myApp>"
// "<myApp> John saying hello"
// "Search for messages in <myApp>"


@implementation IntentHandler

- (id)handlerForIntent:(INIntent *)intent {
    
    if ([intent isKindOfClass:[INTransferMoneyIntent class]]) {
        EXTTransferMoneyIntentHandler * handler = [[EXTTransferMoneyIntentHandler alloc] init];
        handler.expenseService = [[EXTExpenseService alloc] init];
        return handler;
    } else if ([intent isKindOfClass:[INPayBillIntent class]]) {
        EXTPayBillIntentHandler *handler = [[EXTPayBillIntentHandler alloc] init];
        return handler;
    } else if ([intent isKindOfClass:[INSendPaymentIntent class]]) {
        EXTSendPaymentIntentHandler *handler = [[EXTSendPaymentIntentHandler alloc] init];
        handler.expenseService = [[EXTExpenseService alloc] init];
        return handler;
        
    } else {
        return nil;
    }
}


@end
