//
//  IntentViewController.m
//  ExpenseTrackerIntentExtensionUI
//
//  Created by sbt-klochkov-dv on 20/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "IntentViewController.h"
#import <Intents/Intents.h>
#import "EXTUtils.h"

// As an example, this extension's Info.plist has been configured to handle interactions for INSendMessageIntent.
// You will want to replace this or add other intents as appropriate.
// The intents whose interactions you wish to handle must be declared in the extension's Info.plist.

// You can test this example integration by saying things to Siri like:
// "Send a message using <myApp>"

@interface IntentViewController ()
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentsLabel;

@end

@implementation IntentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - INUIHostedViewControlling

// Prepare your view controller for the interaction to handle.
- (void)configureViewForParameters:(NSSet <INParameter *> *)parameters ofInteraction:(INInteraction *)interaction interactiveBehavior:(INUIInteractiveBehavior)interactiveBehavior context:(INUIHostedViewContext)context completion:(void (^)(BOOL success, NSSet <INParameter *> *configuredParameters, CGSize desiredSize))completion {
    // Do configuration here, including preparing views and calculating a desired size for presentation.
    

    if([interaction.intentResponse isKindOfClass: [INTransferMoneyIntentResponse class]]) {
        
        if (parameters.count == 0) {
            // empty parameters set means that the system gives us a chance to display all the intent's parameters at once.
            
            INTransferMoneyIntentResponse * response = (INTransferMoneyIntentResponse *) interaction.intentResponse;
            
            NSSet <INParameter *> * configuredParams = [NSSet setWithArray:@[
                                                                             [INParameter parameterForClass:[INTransferMoneyIntentResponse class] keyPath:@"toAccount"],
                                                                             [INParameter parameterForClass:[INTransferMoneyIntentResponse class] keyPath:@"transactionAmount"],
                                                                             [INParameter parameterForClass:[INTransferMoneyIntentResponse class] keyPath:@"transactionNote"]
                                                                             ]];
            
            if (response.code == INTransferMoneyIntentResponseCodeReady || response.code == INTransferMoneyIntentResponseCodeSuccess) {
                // configure confirmation view
                
                NSString * currencySymbol = [EXTUtils findCurrencySymbolByCode:[[response.transactionAmount amount] currencyCode]];
                
                _amountLabel.text = [NSString stringWithFormat:@"%@ %@", [[[response.transactionAmount amount] amount] stringValue], currencySymbol] ;
                _categoryLabel.text = [response.toAccount.organizationName.spokenPhrase capitalizedString];
                _commentsLabel.text = response.transactionNote;
            }
            
            if (completion) {
                completion(YES, configuredParams, [self desiredSize]);
            }
            
        } else {
            if (completion) {
                // Hide all other views
                // non-zero size is set on purpose. If we set CGSizeZero, the default view will be shown
                completion(YES, parameters, CGSizeMake(0.001, 0.001));
            }
        }

    } else if ([interaction.intentResponse isKindOfClass: [INSendPaymentIntentResponse class]]) {
        
        if (parameters.count == 0) {
            // empty parameters set means that the system gives us a chance to display all the intent's parameters at once.
            
            INSendPaymentIntentResponse * response = (INSendPaymentIntentResponse *) interaction.intentResponse;
            
            NSSet <INParameter *> * configuredParams = [NSSet setWithArray:@[
                                                                             [INParameter parameterForClass:[INSendPaymentIntentResponse class] keyPath:@"paymentRecord.note"],
                                                                             [INParameter parameterForClass:[INSendPaymentIntentResponse class] keyPath:@"paymentRecord.currencyAmount"]
                                                                             ]];
            
            if (response.code == INTransferMoneyIntentResponseCodeReady || response.code == INTransferMoneyIntentResponseCodeSuccess) {
                // configure confirmation view
                
                NSString * currencySymbol = [EXTUtils findCurrencySymbolByCode:[response.paymentRecord.currencyAmount currencyCode]];
                
                _amountLabel.text = [NSString stringWithFormat:@"%@ %@", [[response.paymentRecord.currencyAmount amount] stringValue], currencySymbol] ;
                _categoryLabel.text = response.paymentRecord.note;
            }
            
            if (completion) {
                completion(YES, configuredParams, [self desiredSize]);
            }
            
        } else {
            if (completion) {
                // Hide all other views
                // non-zero size is set on purpose. If we set CGSizeZero, the default view will be shown
                completion(YES, parameters, CGSizeMake(0.001, 0.001));
            }
        }
        
        
    } else {
        completion(YES, parameters, CGSizeMake(0.001, 0.001));
    }

}

- (CGSize)desiredSize {
    
    CGSize size = [self extensionContext].hostedViewMaximumAllowedSize;
    size.height = 119;
    
    return size;
}

- (BOOL)displaysPaymentTransaction {
    return NO;
}

-(BOOL)displaysMessage {
    return NO;
}

@end
