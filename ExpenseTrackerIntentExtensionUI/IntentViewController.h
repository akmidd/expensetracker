//
//  IntentViewController.h
//  ExpenseTrackerIntentExtensionUI
//
//  Created by sbt-klochkov-dv on 20/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <IntentsUI/IntentsUI.h>

@interface IntentViewController : UIViewController <INUIHostedViewControlling, INUIHostedViewSiriProviding>

@end
