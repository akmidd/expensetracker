//
//  EXTExpenseCategory.h
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EXTExpenseCategoryProtocol<NSObject>

@property(nonatomic, copy) NSString * name;
@property(nonatomic, strong) NSUUID * uuid;

@end

@interface EXTExpenseCategory : NSObject<EXTExpenseCategoryProtocol>


@end
