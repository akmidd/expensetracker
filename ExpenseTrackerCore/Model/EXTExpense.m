//
//  EXTExpenseModel.m
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTExpense.h"


@implementation EXTExpense

@synthesize category = _category;
@synthesize amount = _amount;
@synthesize uuid = _uuid;
@synthesize createDate = _createDate;
@synthesize note = _note;

@end
