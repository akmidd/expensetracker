//
//  EXTExpenseProtocol.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 15/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

@protocol EXTExpenseCategoryProtocol;

@protocol EXTExpenseProtocol<NSObject>

@property(nonatomic, strong) NSDecimalNumber * amount;
@property(nonatomic, strong) id<EXTExpenseCategoryProtocol> category;
@property(nonatomic, strong) NSString * note;
@property(nonatomic, strong) NSUUID * uuid;
@property(nonatomic, strong) NSDate *createDate;

@end
