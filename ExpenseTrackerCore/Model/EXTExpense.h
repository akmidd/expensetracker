//
//  EXTExpenseModel.h
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXTExpenseProtocol.h"


@interface EXTExpense : NSObject<EXTExpenseProtocol>

@end
