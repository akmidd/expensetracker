//
//  EXTExpenseService.m
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTExpenseService.h"
#import "EXTExpenseRepository.h"
#import "EXTExpenseCategoryRepository.h"
#import <CoreData/CoreData.h>
#import "EXTExpenseEntity+CoreDataClass.h"
#import "EXTExpenseCategoryEntity+CoreDataClass.h"
#import "EXTExpense.h"

@implementation EXTExpenseService {
    NSPersistentContainer * _persistentContainer;
    EXTExpenseRepository * _expenseRepository;
    EXTExpenseCategoryRepository * _expenseCategoryRepository;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        NSURL * momUrl = [bundle URLForResource:@"ExpenseTracker" withExtension:@"momd"];
        NSManagedObjectModel * mom = [[NSManagedObjectModel alloc] initWithContentsOfURL:momUrl];
        
        NSURL *dbfileUrl = [[[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:@"group.accounting"] URLByAppendingPathComponent:@"Expenses.sqlite"];
        
        _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"ExpenseTracker" managedObjectModel:mom];
        _persistentContainer.persistentStoreDescriptions = @[[NSPersistentStoreDescription persistentStoreDescriptionWithURL:dbfileUrl]];
        
        [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription * _Nonnull description, NSError * _Nullable error) {
            if (error != nil) {
                NSLog(@"Failed to load Core Data stack: %@", error);
                abort();
            }
            
            _expenseRepository = [[EXTExpenseRepository alloc] initWithContext:_persistentContainer.viewContext];
            _expenseCategoryRepository = [[EXTExpenseCategoryRepository alloc] initWithContext:_persistentContainer.viewContext];
            
            [self insertMockData];
            
        }];

    }
    return self;
}

-(void) insertMockData {
    
    //[self clearAllData];
    
    NSUInteger categoriesCount = [_expenseCategoryRepository getAllCategoriesCount];

    if (categoriesCount == 0) {
        
        [_expenseCategoryRepository createNewCategoryWithName:@"Транспорт"];
        [_expenseCategoryRepository createNewCategoryWithName:@"Еда"];
        [_expenseCategoryRepository createNewCategoryWithName:@"Кино"];

    }
}

-(void) clearAllData {
    [_expenseRepository removeAll];
    [_expenseCategoryRepository removeAll];
}


-(void)saveExpense:(id<EXTExpenseProtocol>)expense {
    
    EXTExpenseEntity * entity;
    
    if (expense.uuid != nil) {
        entity = [_expenseRepository getExpenseByUuid:expense.uuid];
    } else {
        entity = [_expenseRepository createNewExpense];
    }
    
    entity.amount = expense.amount;
    entity.category = [_expenseCategoryRepository getExpenseCategoryByUuid:expense.category.uuid];
    entity.note = expense.note;
    entity.createDate = expense.createDate ? expense.createDate : [NSDate date];
    
    [_persistentContainer.viewContext save:nil];
}

-(NSInteger) getAllExpensesCount {
    return [_expenseRepository getAllExpensesCount];
}

-(NSArray<id<EXTExpenseProtocol>> *)getAllExpenses {
    NSMutableArray<EXTExpense *> * expenses = [NSMutableArray new];
    
    NSArray<EXTExpenseEntity *> * entities = [_expenseRepository getAllExpenses];
    
    for (EXTExpenseEntity * entity in entities) {
        [expenses addObject:[self createExpenseModelFromEntity:entity]];
    }
    
    return expenses;
}

-(NSArray<NSNumber *>*)getExpensesWeekGroupsNumber {
    NSMutableArray<NSNumber *>*weekNumbers = [NSMutableArray array];
    NSArray<id<EXTExpenseProtocol>> *expenses = [self getAllExpenses];
    NSInteger secondsInWeek = 60*60*24*7;
    __block NSInteger groupsCount = 0;
    __block NSInteger numberOfItemsInGroup = 0;
    __block NSInteger numberOfWeek = 0;
    __block NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-secondsInWeek * numberOfWeek];
    __block NSDate *endDate = [NSDate dateWithTimeIntervalSinceNow:-secondsInWeek * (numberOfWeek + 1)];
    
    [expenses enumerateObjectsUsingBlock:^(id<EXTExpenseProtocol>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.createDate compare:startDate] == NSOrderedAscending && ([obj.createDate compare:endDate] == NSOrderedDescending || [obj.createDate compare:endDate] == NSOrderedSame)) {
            numberOfItemsInGroup++;
        } else {
            if (numberOfItemsInGroup > 0) {
                groupsCount++;
                [weekNumbers addObject:@(numberOfWeek)];
            }
            while (!([obj.createDate compare:startDate] == NSOrderedAscending && ([obj.createDate compare:endDate] == NSOrderedDescending || [obj.createDate compare:endDate] == NSOrderedSame))) {
                numberOfWeek++;
                startDate = [NSDate dateWithTimeIntervalSinceNow:-secondsInWeek * numberOfWeek];
                endDate = [NSDate dateWithTimeIntervalSinceNow:-secondsInWeek * (numberOfWeek + 1)];
            }
            numberOfItemsInGroup = 1;
        }
    }];
    if (numberOfItemsInGroup > 0) {
        groupsCount++;
        [weekNumbers addObject:@(numberOfWeek)];
    }
    return [weekNumbers copy];
}

-(NSArray<id<EXTExpenseProtocol>> *)getAllExpensesByWeekFromNow:(NSInteger)numberOfWeek {
    NSMutableArray<EXTExpense *> * expenses = [NSMutableArray new];
    
    NSArray<EXTExpenseEntity *> * entities = [_expenseRepository getAllExpensesByWeekFromNow:numberOfWeek];
    
    for (EXTExpenseEntity * entity in entities) {
        [expenses addObject:[self createExpenseModelFromEntity:entity]];
    }
    
    return expenses;
}

-(NSInteger) getAllExpensesCountByWeekFromNow:(NSInteger)numberOfWeek {
    return [_expenseRepository getAllExpensesCountByWeekFromNow:numberOfWeek];
}

-(id<EXTExpenseProtocol>) getExpenseByUuid:(NSUUID *)uuid {
    EXTExpenseEntity * entity = [_expenseRepository getExpenseByUuid:uuid];
    
    return [self createExpenseModelFromEntity:entity];
}

-(id<EXTExpenseProtocol>) createNewExpense {
    
    NSInteger categoryCount = [_expenseCategoryRepository getAllCategoriesCount];
    
    if (categoryCount == 0) {
        NSLog(@"Before adding an expense you should add at least one category");
        return nil;
    }
    
    //get first category for simplicity
    EXTExpenseCategoryEntity * category = [_expenseCategoryRepository getAllCategories][0];

    EXTExpenseEntity *entity = [_expenseRepository createNewExpenseWithCategory:category];
    return [self createExpenseModelFromEntity:entity];
    
}




-(NSInteger) getAllExpenseCategoryCount {
    return [_expenseCategoryRepository getAllCategoriesCount];
}

-(NSArray<id<EXTExpenseCategoryProtocol>> *) getAllExpenseCategories {
    NSMutableArray<EXTExpenseCategory *> * categories = [NSMutableArray new];
    
    NSArray<EXTExpenseCategoryEntity *> * entities = [_expenseCategoryRepository getAllCategories];
    
    for (EXTExpenseCategoryEntity * entity in entities) {
        [categories addObject:[self createExpenseCategoryModelFromEntity:entity]];
    }
    
    return categories;
}

-(id<EXTExpenseCategoryProtocol>) findCategoryByName:(NSString *) name {
    return [self createExpenseCategoryModelFromEntity:[_expenseCategoryRepository findExpenseCategoryByName:name]];
}

-(id<EXTExpenseCategoryProtocol>) createNewCategoryWithName:(NSString *) name {
    
    EXTExpenseCategoryEntity *categoryEntity = [_expenseCategoryRepository findExpenseCategoryByName:name];
    if (categoryEntity == nil) {
        categoryEntity = [_expenseCategoryRepository createNewCategoryWithName:name];
    }
    
    return [self createExpenseCategoryModelFromEntity:categoryEntity];
}



#pragma mark Conversions

-(EXTExpense *) createExpenseModelFromEntity:(EXTExpenseEntity *) entity {
    
    if (entity == nil) {
        return nil;
    }
    
    EXTExpense * model = [[EXTExpense alloc] init];
    
    model.category = [self createExpenseCategoryModelFromEntity:entity.category];
    model.uuid = entity.uuid;
    model.amount = entity.amount;
    model.note = entity.note;
    model.createDate = entity.createDate;
    
    return model;
    
    
}


-(EXTExpenseCategory *) createExpenseCategoryModelFromEntity:(EXTExpenseCategoryEntity *) entity {
    
    if (entity == nil) {
        return nil;
    }
    
    EXTExpenseCategory * model = [[EXTExpenseCategory alloc] init];
    
    model.name = entity.name;
    model.uuid = entity.uuid;
    
    return model;
}



















@end
