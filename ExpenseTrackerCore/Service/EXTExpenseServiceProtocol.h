//
//  EXTExpenseServiceProtocol.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 15/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTExpenseProtocol.h"
#import "EXTExpenseCategory.h"


@protocol EXTExpenseServiceProtocol<NSObject>

#pragma mark Expenses
-(NSInteger) getAllExpensesCount;
-(NSArray<id<EXTExpenseProtocol>> *) getAllExpenses;
-(void) saveExpense:(id<EXTExpenseProtocol>)expense;
-(id<EXTExpenseProtocol>) createNewExpense;
-(id<EXTExpenseProtocol>) getExpenseByUuid:(NSUUID *)uuid;
-(NSArray<NSNumber *>*)getExpensesWeekGroupsNumber;
-(NSArray<id<EXTExpenseProtocol>> *)getAllExpensesByWeekFromNow:(NSInteger)numberOfWeek;
-(NSInteger) getAllExpensesCountByWeekFromNow:(NSInteger)numberOfWeek;

#pragma mark ExpenseCategory
-(NSInteger) getAllExpenseCategoryCount;
-(NSArray<id<EXTExpenseCategoryProtocol>> *) getAllExpenseCategories;
-(id<EXTExpenseCategoryProtocol>) findCategoryByName:(NSString *) name;
-(id<EXTExpenseCategoryProtocol>) createNewCategoryWithName:(NSString *) name;

@end
