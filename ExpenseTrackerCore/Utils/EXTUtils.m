//
//  EXTUtils.m
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 21/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTUtils.h"

@implementation EXTUtils


+ (NSLocale *) findLocaleByCurrencyCode:(NSString *) currencyCode
{
    NSArray *locales = [NSLocale availableLocaleIdentifiers];
    NSLocale *locale = nil;
    
    for (NSString *localeId in locales) {
        locale = [[NSLocale alloc] initWithLocaleIdentifier:localeId];
        NSString *code = [locale objectForKey:NSLocaleCurrencyCode];
        if ([code isEqualToString: currencyCode])
            break;
        else
            locale = nil;
        
    }
    
    return locale;
}

+ (NSString *)findCurrencySymbolByCode:(NSString *) currencyCode
{
    NSNumberFormatter *fmtr = [[NSNumberFormatter alloc] init];
    NSLocale *locale = [self findLocaleByCurrencyCode: currencyCode];
    NSString *currencySymbol;
    if (locale)
        [fmtr setLocale:locale];
    [fmtr setNumberStyle:NSNumberFormatterCurrencyStyle];
    currencySymbol = [fmtr currencySymbol];
    
    if (currencySymbol.length > 1)
        currencySymbol = [currencySymbol substringToIndex:1];
    return currencySymbol;
}


+ (NSString *) getFirstNounFromString:(NSString *) string {
    
     NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes:@[NSLinguisticTagSchemeLexicalClass, NSLinguisticTagSchemeLanguage, NSLinguisticTagSchemeLemma] options:NSLinguisticTaggerOmitPunctuation | NSLinguisticTaggerJoinNames | NSLinguisticTaggerOmitWhitespace];
    
    [tagger setString:string];
    
    NSRange range = NSMakeRange(0, string.length);
    
    [tagger setOrthography:[NSOrthography defaultOrthographyForLanguage:@"ru"] range:range];
    
    __block NSString * firstNounWord;
    
    [tagger enumerateTagsInRange:range unit:(NSLinguisticTaggerUnitWord) scheme:NSLinguisticTagSchemeLexicalClass options:NSLinguisticTaggerOmitPunctuation | NSLinguisticTaggerJoinNames | NSLinguisticTaggerOmitWhitespace usingBlock:^(NSLinguisticTag  _Nullable tag, NSRange tokenRange, BOOL * _Nonnull stop) {
        if (tag == NSLinguisticTagNoun) {
            firstNounWord = [string substringWithRange:tokenRange];
            *stop = YES;
            
            [tagger enumerateTagsInRange:tokenRange unit:(NSLinguisticTaggerUnitWord) scheme:NSLinguisticTagSchemeLemma options:0 usingBlock:^(NSLinguisticTag  _Nullable tag, NSRange tokenRange, BOOL * _Nonnull stop) {
                if (tag != nil) {
                    firstNounWord = [tag capitalizedString];
                    *stop = YES;
                }
            }];
        }
    }];
    
    return firstNounWord;
    
}

@end
