//
//  EXTUtils.h
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 21/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EXTUtils : NSObject

+ (NSLocale *) findLocaleByCurrencyCode:(NSString *) currencyCode;
+ (NSString *) findCurrencySymbolByCode:(NSString *) currencyCode;

// NLP

+ (NSString *) getFirstNounFromString:(NSString *) string;

@end
