//
//  EXTExpenseRepository.m
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTExpenseRepository.h"

@implementation EXTExpenseRepository



-(EXTExpenseEntity *) getExpenseByUuid:(NSUUID *)uuid {
    NSFetchRequest * request = [EXTExpenseEntity fetchRequest];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uuid = %@", uuid];

    [request setPredicate:predicate];
    
    NSArray * resultArray = [self.moc executeFetchRequest:request error:nil];
    
    if (resultArray && resultArray.count > 0) {
        return (EXTExpenseEntity *) resultArray[0];
    } else {
        return nil;
    }
    
}

-(EXTExpenseEntity *)createNewExpense {
    return [self createNewExpenseWithCategory:nil];

}

-(EXTExpenseEntity *)createNewExpenseWithCategory:(EXTExpenseCategoryEntity *) category {
    EXTExpenseEntity * expenseEntity = [[EXTExpenseEntity alloc] initWithContext:self.moc];
    expenseEntity.category = category;
    expenseEntity.uuid = [NSUUID UUID];
    expenseEntity.createDate = [NSDate date];
    
    NSError * error;
    
    [self.moc save:&error];
    
    if (error) {
        NSLog(@"Error while trying to save new Expense entity: %@", error);
        return nil;
    } else {
        return expenseEntity;
    }

}

-(NSUInteger) getAllExpensesCount {
    NSAssert(self.moc != nil, kNilMocError);
    
    NSFetchRequest * request = [EXTExpenseEntity fetchRequest];
    return [self.moc countForFetchRequest:request error:nil];
}


-(NSArray<EXTExpenseEntity *> *)getAllExpenses {
    NSAssert(self.moc != nil, kNilMocError);
    
    NSFetchRequest * request = [EXTExpenseEntity fetchRequest];
    request.sortDescriptors =@[[NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:NO]];
    
    NSArray * result = [self.moc executeFetchRequest:request error:nil];
    
    return result;
}

-(NSArray<EXTExpenseEntity *> *)getAllExpensesByWeekFromNow:(NSInteger)numberOfWeek {
    NSAssert(self.moc != nil, kNilMocError);
    
    NSFetchRequest * request = [EXTExpenseEntity fetchRequest];
    
    NSInteger secondsInWeek = 60*60*24*7;
    NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-secondsInWeek * numberOfWeek];
    NSDate *endDate = [NSDate dateWithTimeIntervalSinceNow:-secondsInWeek * (numberOfWeek + 1)];
    request.predicate = [NSPredicate predicateWithFormat:@"(createDate >= %@) AND (createDate <= %@)", endDate, startDate];
    request.sortDescriptors =@[[NSSortDescriptor sortDescriptorWithKey:@"createDate" ascending:NO]];
    
    NSArray * result = [self.moc executeFetchRequest:request error:nil];
    
    return result;
}

-(NSUInteger) getAllExpensesCountByWeekFromNow:(NSInteger)numberOfWeek {
    NSAssert(self.moc != nil, kNilMocError);
    
    NSFetchRequest * request = [EXTExpenseEntity fetchRequest];
    
    NSInteger secondsInWeek = 60*60*24*7;
    NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-secondsInWeek * numberOfWeek];
    NSDate *endDate = [NSDate dateWithTimeIntervalSinceNow:-secondsInWeek * (numberOfWeek + 1)];
    request.predicate = [NSPredicate predicateWithFormat:@"(createDate >= %@) AND (createDate <= %@)", endDate, startDate];
    
    return [self.moc countForFetchRequest:request error:nil];
}

-(void) saveExpense:(EXTExpenseEntity *)expense {
    NSAssert(self.moc != nil, kNilMocError);

    if (!expense.uuid) {
        expense.uuid = [NSUUID UUID];
    }
    
    [self.moc save:nil];
}

-(void) removeAll {
    NSArray<EXTExpenseEntity *> * allExpenses = [self getAllExpenses];
    
    for (EXTExpenseEntity * entity in allExpenses) {
        [self.moc deleteObject:entity];
    }
    
    [self.moc save:nil];
    
}



@end
