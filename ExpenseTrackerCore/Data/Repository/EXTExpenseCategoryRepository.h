//
//  EXTExpenseCategoryRepository.h
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXTRepository.h"
#import "EXTExpenseCategoryEntity+CoreDataClass.h"

@interface EXTExpenseCategoryRepository: EXTRepository

-(EXTExpenseCategoryEntity *) getExpenseCategoryByUuid:(NSUUID *)uuid;
-(EXTExpenseCategoryEntity *) findExpenseCategoryByName:(NSString *)name;

-(NSInteger) getAllCategoriesCount;
-(NSArray<EXTExpenseCategoryEntity *> *) getAllCategories;
-(void) saveCategory:(EXTExpenseCategoryEntity *) category;
-(void) removeAll;
-(EXTExpenseCategoryEntity *)createNewCategoryWithName:(NSString*)name;

@end
