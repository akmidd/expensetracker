//
//  EXTExpenseCategoryRepository.m
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTExpenseCategoryRepository.h"

@implementation EXTExpenseCategoryRepository


-(EXTExpenseCategoryEntity *) getExpenseCategoryByUuid:(NSUUID *)uuid {
    NSFetchRequest * request = [EXTExpenseCategoryEntity fetchRequest];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"uuid == %@", uuid];
    
    [request setPredicate:predicate];
    
    NSArray * resultArray = [self.moc executeFetchRequest:request error:nil];
    
    if (resultArray && resultArray.count > 0) {
        return (EXTExpenseCategoryEntity *) resultArray[0];
    } else {
        return nil;
    }
}

-(EXTExpenseCategoryEntity *) findExpenseCategoryByName:(NSString *)name {
    NSFetchRequest * request = [EXTExpenseCategoryEntity fetchRequest];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"name ==[c] %@", name];
    
    [request setPredicate:predicate];
    
    NSArray * resultArray = [self.moc executeFetchRequest:request error:nil];
    
    if (resultArray && resultArray.count > 0) {
        return (EXTExpenseCategoryEntity *) resultArray[0];
    } else {
        return nil;
    }
}

-(NSInteger)getAllCategoriesCount {
    NSAssert(self.moc != nil, kNilMocError);
    
    NSFetchRequest * request = [EXTExpenseCategoryEntity fetchRequest];
    return [self.moc countForFetchRequest:request error:nil];
}

-(NSArray<EXTExpenseCategoryEntity *> *)getAllCategories {
    NSAssert(self.moc != nil, kNilMocError);
    
    NSFetchRequest * request = [EXTExpenseCategoryEntity fetchRequest];
    
    NSArray * result = [self.moc executeFetchRequest:request error:nil];
    
    return result;
}

-(void) saveCategory:(EXTExpenseCategoryEntity *) category {
    NSAssert(self.moc != nil, kNilMocError);
    
    if (!category.uuid) {
        category.uuid = [NSUUID UUID];
    }
    
    [self.moc insertObject:category];
    [self.moc save:nil];
}

-(void) removeAll {
    NSArray<EXTExpenseCategoryEntity *> * allExpenses = [self getAllCategories];
    
    for (EXTExpenseCategoryEntity * entity in allExpenses) {
        [self.moc deleteObject:entity];
    }
    
    [self.moc save:nil];
    
}

-(EXTExpenseCategoryEntity *)createNewCategoryWithName:(NSString*)name {
    
    EXTExpenseCategoryEntity * entity = [[EXTExpenseCategoryEntity alloc] initWithContext:self.moc];
    entity.name = name;
    entity.uuid = [NSUUID UUID];
    [self.moc save:nil];
    
    return entity;
}

@end
