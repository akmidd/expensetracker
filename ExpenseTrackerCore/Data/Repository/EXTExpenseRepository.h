//
//  EXTExpenseRepository.h
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXTRepository.h"


#import "EXTExpenseEntity+CoreDataClass.h"

@interface EXTExpenseRepository : EXTRepository

-(NSArray<EXTExpenseEntity *> *)getAllExpenses;
-(NSArray<EXTExpenseEntity *> *)getAllExpensesByWeekFromNow:(NSInteger)numberOfWeek;
-(NSUInteger) getAllExpensesCount;
-(NSUInteger) getAllExpensesCountByWeekFromNow:(NSInteger)numberOfWeek;
-(void) saveExpense:(EXTExpenseEntity *)expense;
-(EXTExpenseEntity *) getExpenseByUuid:(NSUUID *)uuid;
-(EXTExpenseEntity *)createNewExpense;
-(EXTExpenseEntity *)createNewExpenseWithCategory:(EXTExpenseCategoryEntity *) category;
-(void) removeAll;

@end
