//
//  EXTRepository.m
//  ExpenseTrackerCore
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXTRepository.h"

NSString *const kNilMocError = @"property moc(NSManagedObjectContext) should be set before using the repository methods";

@implementation EXTRepository

-(instancetype)initWithContext:(NSManagedObjectContext *)moc {
    if (self = [super init]) {
        _moc = moc;
    }
    return self;
}

@end
