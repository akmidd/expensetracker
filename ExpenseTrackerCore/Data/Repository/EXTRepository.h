//
//  EXTRepository.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <CoreData/CoreData.h>

extern NSString * const kNilMocError;


@interface EXTRepository : NSObject

@property (nonatomic, strong) NSManagedObjectContext * moc;

-(instancetype)initWithContext:(NSManagedObjectContext *) moc;

@end
