//
//  AppDelegate.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "AppDelegate.h"
#import "AppDelegate+Siri.h"
#import <RamblerTyphoonUtils/RamblerInitialAssemblyCollector.h>
#import "EXTApplicationStateTrackerManagerProtocol.h"
#import "MBFingerTipWindow.h"
#import <CoreSpotlight/CoreSpotlight.h>
#import <MobileCoreServices/MobileCoreServices.h>


@interface AppDelegate ()

@property (nonatomic, strong) id<EXTApplicationStateTrackerManagerProtocol> trackingManager;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [self requestSiriAuthorization];
    [self activateSpotlight];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    [self.trackingManager applicationWillResignActive:application];
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self.trackingManager applicationDidEnterBackground:application];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [self.trackingManager applicationWillEnterForeground:application];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self.trackingManager applicationDidBecomeActive:application];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self.trackingManager applicationWillTerminate:application];
}

#pragma mark - TyphoonInitializer

- (NSArray *)initialAssemblies {
    RamblerInitialAssemblyCollector *collector = [RamblerInitialAssemblyCollector new];
    return [collector collectInitialAssemblyClasses];
}

-(BOOL)application:(UIApplication *)application willContinueUserActivityWithType:(NSString *)userActivityType {
    NSLog(@"launched from siri");
    return true;
}

-(BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler {
    NSLog(@"launched from siri");
    return true;
}

// Uncomment to enable touch indicators
//- (UIWindow *)window {
//    if (!_window) {
//        MBFingerTipWindow * window = [[MBFingerTipWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
//        window.alwaysShowTouches = YES;
//        _window = window;
//    }
//    return _window;
//}

- (void)activateSpotlight {
    CSSearchableItemAttributeSet *attributeSet;
    attributeSet = [[CSSearchableItemAttributeSet alloc]
                    initWithItemContentType:(NSString *)kUTTypeImage];
    
    attributeSet.title = @"Учет расходов";
    attributeSet.contentDescription = @"Голосовой помощник";
    
    attributeSet.keywords = @[@"Потратил", @"потратил", @"Учет расходов", @"Помощник"];
    
    UIImage *image = [UIImage imageNamed:@"AppIcon.png"];
    NSData *imageData = [NSData dataWithData:UIImagePNGRepresentation(image)];
    attributeSet.thumbnailData = imageData;
    
    CSSearchableItem *item = [[CSSearchableItem alloc]
                              initWithUniqueIdentifier:@"com.deeplink"
                              domainIdentifier:@"spotlight.sample"
                              attributeSet:attributeSet];
    
    [[CSSearchableIndex defaultSearchableIndex] indexSearchableItems:@[item]
                                                   completionHandler: ^(NSError * __nullable error) {
                                                       if (!error)
                                                       NSLog(@"Search item indexed");
                                                   }];
}

@end
