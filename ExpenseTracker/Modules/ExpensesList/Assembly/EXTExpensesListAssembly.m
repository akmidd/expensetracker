//
//  EXTExpensesListAssembly.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpensesListAssembly.h"

#import "EXTExpensesListViewController.h"
#import "EXTExpensesListInteractor.h"
#import "EXTExpensesListPresenter.h"
#import "EXTExpensesListRouter.h"
#import "EXTExpenseCellDecorator.h"
#import "EXTExpenseCellDecoratorProtocol.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "EXTApplicationStateTrackerManagerProtocol.h"
#import "EXTAppStateAssembly.h"

@interface EXTExpensesListAssembly()

@property (nonatomic, weak) EXTAppStateAssembly *appStateAssembly;

@end

@implementation EXTExpensesListAssembly

- (EXTExpensesListViewController *)viewExpensesList {
    return [TyphoonDefinition withClass:[EXTExpensesListViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterExpensesList]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterExpensesList]];
                              [definition injectProperty:@selector(expenseService)
                                                    with:[self.globalAssembly expenseService]];
                              [definition injectProperty:@selector(cellDecorator)
                                                    with:[self cellDecoratorExpenseList]];
                          }];
}

- (EXTExpensesListInteractor *)interactorExpensesList {
    return [TyphoonDefinition withClass:[EXTExpensesListInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterExpensesList]];
                          }];
}

- (EXTExpensesListPresenter *)presenterExpensesList{
    return [TyphoonDefinition withClass:[EXTExpensesListPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewExpensesList]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorExpensesList]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerExpensesList]];
                              [definition injectProperty:@selector(expenseService)
                                                    with:[self.globalAssembly expenseService]];
                              [definition injectProperty:@selector(tracker)
                                                    with:[self.appStateAssembly appStateTrackingManager]];
                          }];
}

- (EXTExpensesListRouter *)routerExpensesList{
    return [TyphoonDefinition withClass:[EXTExpensesListRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewExpensesList]];
                          }];
}


- (id<EXTExpenseCellDecoratorProtocol>)cellDecoratorExpenseList {
    return [TyphoonDefinition withClass:[EXTExpenseCellDecorator class]];
}

@end


