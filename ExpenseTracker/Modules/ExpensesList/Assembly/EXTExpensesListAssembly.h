//
//  EXTExpensesListAssembly.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "EXTAsembly.h"

/**
 @author sbt-klochkov-dv

 ExpensesList module
 */
@interface EXTExpensesListAssembly : TyphoonAssembly <RamblerInitialAssembly>

@property(nonatomic, strong) EXTAsembly * globalAssembly;

@end
