//
//  EXTExpensesListInteractor.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpensesListInteractorInput.h"

@protocol EXTExpensesListInteractorOutput;

@interface EXTExpensesListInteractor : NSObject <EXTExpensesListInteractorInput>

@property (nonatomic, weak) id<EXTExpensesListInteractorOutput> output;

@end
