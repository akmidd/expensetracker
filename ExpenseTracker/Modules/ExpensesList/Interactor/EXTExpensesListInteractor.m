//
//  EXTExpensesListInteractor.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpensesListInteractor.h"

#import "EXTExpensesListInteractorOutput.h"

@implementation EXTExpensesListInteractor

#pragma mark - Методы EXTExpensesListInteractorInput

- (NSString *)title {
//    return @"Личный бухгалтер";
    return @"Расходы";
}

@end
