//
//  EXTExpensesListRouter.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpensesListRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "EXTExpenseDetailsEditModuleInput.h"
#import "EXTExpenseDetailsEditViewController.h"

@implementation EXTExpensesListRouter

#pragma mark - Методы EXTExpensesListRouterInput


- (void)openExpenseWithUuid:(NSUUID *)uuid fromViewController:(UIViewController*)fromVC{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ExpenseDetailsEdit" bundle:[NSBundle bundleForClass:[self class]]];
    
    EXTExpenseDetailsEditViewController *viewController = (EXTExpenseDetailsEditViewController *) [storyboard instantiateInitialViewController];
    id<EXTExpenseDetailsEditModuleInput> detailsModuleInput = (id<EXTExpenseDetailsEditModuleInput>) viewController.output;
    detailsModuleInput.expenseUuid = uuid;
    
    [fromVC.navigationController pushViewController:viewController animated:YES];
}

- (void)openExpenseForCreateFromViewController:(UIViewController*)fromVC{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ExpenseDetailsCreate" bundle:[NSBundle bundleForClass:[self class]]];
    EXTExpenseDetailsEditViewController *viewController = (EXTExpenseDetailsEditViewController *) [storyboard instantiateInitialViewController];
    [fromVC.navigationController pushViewController:viewController animated:YES];
}

@end
