//
//  EXTExpensesListRouter.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpensesListRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface EXTExpensesListRouter : NSObject <EXTExpensesListRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
