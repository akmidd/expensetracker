//
//  EXTExpensesListRouterInput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EXTExpensesListRouterInput <NSObject>

@required
-(void)openExpenseWithUuid:(NSUUID*)uuid fromViewController:(UIViewController*)fromVC;
-(void)openExpenseForCreateFromViewController:(UIViewController*)fromVC;

@end
