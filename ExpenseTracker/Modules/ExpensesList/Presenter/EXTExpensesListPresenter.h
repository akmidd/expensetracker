//
//  EXTExpensesListPresenter.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpensesListViewOutput.h"
#import "EXTExpensesListInteractorOutput.h"
#import "EXTExpensesListModuleInput.h"
#import "EXTExpenseServiceProtocol.h"

@protocol EXTExpensesListViewInput;
@protocol EXTExpensesListInteractorInput;
@protocol EXTExpensesListRouterInput;
@protocol EXTApplicationStateTrackerManagerProtocol;

@interface EXTExpensesListPresenter : NSObject <EXTExpensesListModuleInput, EXTExpensesListViewOutput, EXTExpensesListInteractorOutput>

@property (nonatomic, weak) id<EXTExpensesListViewInput> view;
@property (nonatomic, strong) id<EXTExpensesListInteractorInput> interactor;
@property (nonatomic, strong) id<EXTExpensesListRouterInput> router;

@property(nonatomic, strong) id<EXTExpenseServiceProtocol> expenseService;
@property (nonatomic, strong, readonly) id<EXTApplicationStateTrackerManagerProtocol> tracker;


@end
