//
//  EXTExpensesListPresenter.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpensesListPresenter.h"

#import "EXTExpensesListViewInput.h"
#import "EXTExpensesListInteractorInput.h"
#import "EXTExpensesListRouterInput.h"
#import "EXTApplicationStateChangesProtocol.h"
#import "EXTApplicationStateTrackerManagerProtocol.h"


@interface EXTExpensesListPresenter() <EXTApplicationStateChangesProtocol>

@property (nonatomic, strong) id<EXTApplicationStateTrackerManagerProtocol> tracker;

@end


@implementation EXTExpensesListPresenter

#pragma mark - Методы EXTExpensesListModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы EXTExpensesListViewOutput

- (void)setTracker:(id<EXTApplicationStateTrackerManagerProtocol>)tracker {
    _tracker = tracker;
    [tracker addTracker:self];
}

#pragma mark - <EXTApplicationStateChangesProtocol>

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [self.view update];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

- (void)didTriggerViewWillAppearEvent {
    [self.view update];
}

-(void)addButtonTapped {
    //id<EXTExpenseProtocol> expense = [self.expenseService createNewExpense];
//    [self.router openExpenseWithUuid:expense.uuid fromViewController:(UIViewController*)self.view];
    [self.router openExpenseForCreateFromViewController:(UIViewController*)self.view];
}

-(void) didTapOnExpenseWithUuid:(NSUUID *) uuid {
    [self.router openExpenseWithUuid:uuid fromViewController:(UIViewController*)self.view];
}

- (NSString *)title {
    return [self.interactor title];
}

#pragma mark - Методы EXTExpensesListInteractorOutput

@end
