//
//  EXTExpensesListViewOutput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TitleProtocol.h"

@protocol EXTExpensesListViewOutput <TitleProtocol>

/**
 @author sbt-klochkov-dv

 Метод сообщает презентеру о том, что view готова к работе
 */

@required
-(void)didTriggerViewReadyEvent;
-(void)didTriggerViewWillAppearEvent;
-(void)addButtonTapped;
-(void)didTapOnExpenseWithUuid:(NSUUID *) uuid;

@end
