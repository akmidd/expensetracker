//
//  EXTExpenseHeaderView.h
//  ExpenseTracker
//
//  Created by Maxim on 19/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EXTExpenseHeaderView : UITableViewHeaderFooterView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountlabel;

@end
