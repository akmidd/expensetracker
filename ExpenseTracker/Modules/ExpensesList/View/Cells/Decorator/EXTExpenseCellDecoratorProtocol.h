//
//  EXTExpenseCellDecoratorProtocol.h
//  ExpenseTracker
//
//  Created by Maxim on 19/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>

@class EXTExpenseTableViewCell;
@class EXTExpenseHeaderView;
@protocol EXTExpenseProtocol;

@protocol EXTExpenseCellDecoratorProtocol <NSObject>

- (void)decorateCell:(EXTExpenseTableViewCell *)cell withExpense:(id<EXTExpenseProtocol>)expense;
- (void)decorateHeader:(EXTExpenseHeaderView *)headerView section:(NSInteger)section amount:(NSString *)amount;

@end
