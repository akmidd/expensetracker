//
//  EXTExpenseCellDecorator.m
//  ExpenseTracker
//
//  Created by Maxim on 19/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTExpenseCellDecorator.h"
#import "EXTExpenseCellDecoratorProtocol.h"
#import "EXTExpenseTableViewCell.h"
#import "EXTExpenseProtocol.h"
#import "EXTExpenseCategory.h"
#import "EXTExpenseHeaderView.h"

@interface EXTExpenseCellDecorator () <EXTExpenseCellDecoratorProtocol>

@end

@implementation EXTExpenseCellDecorator

- (void)decorateCell:(EXTExpenseTableViewCell *)cell withExpense:(id<EXTExpenseProtocol>)expense {
    cell.titleLabel.text = expense.category.name;
    NSString *amount = [NSString stringWithFormat:@"%.2f", expense.amount.floatValue];
    cell.amountLabel.text = [amount stringByAppendingString:@" \u20bd"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    NSString *formattedDateString = [dateFormatter stringFromDate:expense.createDate];
    cell.dateLabel.text = formattedDateString;
}

- (void)decorateHeader:(EXTExpenseHeaderView *)headerView section:(NSInteger)section amount:(NSString *)amount {
    if (section == 0) {
        headerView.titleLabel.text = @"За неделю";
    } else {
        NSInteger weekTime = 60*60*24*7;
        NSDate *startDate = [NSDate dateWithTimeIntervalSinceNow:-weekTime * (section + 1)];
        NSDate *endDate = [NSDate dateWithTimeIntervalSinceNow:-weekTime * section];
        
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay fromDate:startDate];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd MMMM";
        
        NSString *startDateString = [NSString stringWithFormat:@"%li", components.day];
        NSString *endDateString = [dateFormatter stringFromDate:endDate];
        
        headerView.titleLabel.text = [NSString stringWithFormat:@"%@ - %@", startDateString, endDateString];
    }
    headerView.amountlabel.text = [amount stringByAppendingString:@" \u20bd"];;
}

@end
