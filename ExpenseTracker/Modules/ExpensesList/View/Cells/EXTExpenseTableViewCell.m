//
//  EXTExpenseTableViewCell.m
//  ExpenseTracker
//
//  Created by Maxim on 19/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTExpenseTableViewCell.h"

@implementation EXTExpenseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
