//
//  EXTExpensesListViewController.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpensesListViewController.h"

#import "EXTExpensesListViewOutput.h"

#import "EXTExpense.h"
#import "EXTExpenseCategory.h"
#import "EXTExpenseTableViewCell.h"
#import "EXTExpenseCellDecoratorProtocol.h"
#import "EXTExpenseHeaderView.h"

NSString * const kEXTExpenseCellIdentifier = @"EXTExpenseCell";

@interface EXTExpensesListViewController ()

@property (nonatomic, strong) NSArray<NSNumber *> *weekNumbers;

@end

@implementation EXTExpensesListViewController

@synthesize expenseService;

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    self.title = [self.output title];
	[self.output didTriggerViewReadyEvent];
    [self sbt_registerCells];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update) name:UIApplicationDidBecomeActiveNotification object:self];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(update) name:UIApplicationWillEnterForegroundNotification object:self];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.output didTriggerViewWillAppearEvent];
}

- (void)sbt_registerCells {
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EXTExpenseTableViewCell class]) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:NSStringFromClass([EXTExpenseTableViewCell class])];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([EXTExpenseHeaderView class]) bundle:[NSBundle mainBundle]] forHeaderFooterViewReuseIdentifier:NSStringFromClass([EXTExpenseHeaderView class])];
}

#pragma mark - Методы EXTExpensesListViewInput

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
}

-(void)update {
    [self.tableView reloadData];
}

- (IBAction)addExpenseButtonTapped:(UIBarButtonItem *)sender {
    [self.output addButtonTapped];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.expenseService getAllExpensesCountByWeekFromNow:[self.weekNumbers[section] integerValue]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    self.weekNumbers = [self.expenseService getExpensesWeekGroupsNumber];
    return self.weekNumbers.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    EXTExpenseHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:NSStringFromClass([EXTExpenseHeaderView class])];
    NSArray *expenses = [self.expenseService getAllExpensesByWeekFromNow:[self.weekNumbers[section] integerValue]];
    float totalAmount = 0;
    for (id<EXTExpenseProtocol> expense in expenses) {
        totalAmount += expense.amount.floatValue;
    }
    
    NSString *amount = [NSString stringWithFormat:@"%.2f", totalAmount];
    
    [self.cellDecorator decorateHeader:headerView section:[self.weekNumbers[section] integerValue] amount:amount];
    return headerView;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    EXTExpenseTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EXTExpenseTableViewCell class]) forIndexPath:indexPath];
    id<EXTExpenseProtocol> expense = [self.expenseService getAllExpensesByWeekFromNow:[self.weekNumbers[indexPath.section] integerValue]][indexPath.row];
    [self.cellDecorator decorateCell:cell withExpense:expense];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    id<EXTExpenseProtocol> expense = [self.expenseService getAllExpensesByWeekFromNow:[self.weekNumbers[indexPath.section] integerValue]][indexPath.row];
    [self.output didTapOnExpenseWithUuid:expense.uuid];
}

@end
