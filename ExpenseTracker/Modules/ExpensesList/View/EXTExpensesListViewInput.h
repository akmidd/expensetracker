//
//  EXTExpensesListViewInput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXTExpense.h"
#import "EXTExpenseServiceProtocol.h"

@protocol EXTExpensesListViewInput <NSObject>

@property(nonatomic, strong) id<EXTExpenseServiceProtocol> expenseService;

/**
 @author sbt-klochkov-dv

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

-(void) update;

@end
