//
//  EXTExpensesListViewController.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 07/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EXTExpensesListViewInput.h"

@protocol EXTExpensesListViewOutput;
@protocol EXTExpenseCellDecoratorProtocol;

@interface EXTExpensesListViewController : UITableViewController <EXTExpensesListViewInput>

@property (nonatomic, strong) id<EXTExpensesListViewOutput> output;
@property (nonatomic, strong) id<EXTExpenseCellDecoratorProtocol> cellDecorator;

@end
