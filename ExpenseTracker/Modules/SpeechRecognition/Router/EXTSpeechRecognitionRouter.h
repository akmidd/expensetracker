//
//  EXTSpeechRecognitionRouter.h
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTSpeechRecognitionRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface EXTSpeechRecognitionRouter : NSObject <EXTSpeechRecognitionRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
