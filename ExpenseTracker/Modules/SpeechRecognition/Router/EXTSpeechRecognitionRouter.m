//
//  EXTSpeechRecognitionRouter.m
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTSpeechRecognitionRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation EXTSpeechRecognitionRouter

#pragma mark - Методы EXTSpeechRecognitionRouterInput

- (void)openExpenseListFromViewController:(UIViewController*)fromVC{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"ExpensesList" bundle:[NSBundle bundleForClass:[self class]]];
    UIViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"EXTExpensesListViewController"];
    [fromVC.navigationController pushViewController:viewController animated:YES];
}

@end
