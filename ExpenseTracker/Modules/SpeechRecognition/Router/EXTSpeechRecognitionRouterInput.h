//
//  EXTSpeechRecognitionRouterInput.h
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EXTSpeechRecognitionRouterInput <NSObject>

@required
- (void)openExpenseListFromViewController:(UIViewController*)fromVC;

@end
