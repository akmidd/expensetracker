//
//  EXTSpeechRecognitionInteractor.m
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTSpeechRecognitionInteractor.h"

#import "EXTSpeechRecognitionInteractorOutput.h"

@implementation EXTSpeechRecognitionInteractor

#pragma mark - Методы EXTSpeechRecognitionInteractorInput

- (NSString *)title {
    return @"Личный бухгалтер";
}

@end
