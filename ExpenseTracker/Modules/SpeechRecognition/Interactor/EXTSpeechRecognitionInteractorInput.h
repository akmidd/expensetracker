//
//  EXTSpeechRecognitionInteractorInput.h
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TitleProtocol.h"

@protocol EXTSpeechRecognitionInteractorInput <TitleProtocol>

@end
