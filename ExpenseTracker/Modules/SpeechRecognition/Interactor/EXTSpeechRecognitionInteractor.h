//
//  EXTSpeechRecognitionInteractor.h
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTSpeechRecognitionInteractorInput.h"

@protocol EXTSpeechRecognitionInteractorOutput;

@interface EXTSpeechRecognitionInteractor : NSObject <EXTSpeechRecognitionInteractorInput>

@property (nonatomic, weak) id<EXTSpeechRecognitionInteractorOutput> output;

@end
