//
//  EXTSpeechRecognitionModuleInput.h
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol EXTSpeechRecognitionModuleInput <RamblerViperModuleInput>

/**
 @author paul_arctic

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@end
