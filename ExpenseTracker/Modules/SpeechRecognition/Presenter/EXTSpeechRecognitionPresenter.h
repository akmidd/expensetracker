//
//  EXTSpeechRecognitionPresenter.h
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTSpeechRecognitionViewOutput.h"
#import "EXTSpeechRecognitionInteractorOutput.h"
#import "EXTSpeechRecognitionModuleInput.h"

@protocol EXTSpeechRecognitionViewInput;
@protocol EXTSpeechRecognitionInteractorInput;
@protocol EXTSpeechRecognitionRouterInput;

@interface EXTSpeechRecognitionPresenter : NSObject <EXTSpeechRecognitionModuleInput, EXTSpeechRecognitionViewOutput, EXTSpeechRecognitionInteractorOutput>

@property (nonatomic, weak) id<EXTSpeechRecognitionViewInput> view;
@property (nonatomic, strong) id<EXTSpeechRecognitionInteractorInput> interactor;
@property (nonatomic, strong) id<EXTSpeechRecognitionRouterInput> router;

@end
