//
//  EXTSpeechRecognitionPresenter.m
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTSpeechRecognitionPresenter.h"

#import "EXTSpeechRecognitionViewInput.h"
#import "EXTSpeechRecognitionInteractorInput.h"
#import "EXTSpeechRecognitionRouterInput.h"

@implementation EXTSpeechRecognitionPresenter

#pragma mark - Методы EXTSpeechRecognitionModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы EXTSpeechRecognitionViewOutput

- (NSString *)title {
    return [self.interactor title];
}

- (void)showExpenses {
    [self.router openExpenseListFromViewController:(UIViewController*)self.view];
}

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}

#pragma mark - Методы EXTSpeechRecognitionInteractorOutput


@end
