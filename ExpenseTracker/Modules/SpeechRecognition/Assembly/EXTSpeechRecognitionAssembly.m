//
//  EXTSpeechRecognitionAssembly.m
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTSpeechRecognitionAssembly.h"

#import "EXTSpeechRecognitionViewController.h"
#import "EXTSpeechRecognitionInteractor.h"
#import "EXTSpeechRecognitionPresenter.h"
#import "EXTSpeechRecognitionRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "EXTAsembly.h"
#import "SBTKeyboardControlAssembly.h"

@interface EXTSpeechRecognitionAssembly()

@property(nonatomic, strong) EXTAsembly * globalAssembly;
@property (nonatomic, weak) SBTKeyboardControlAssembly *keyboardAssembly;

@end

@implementation EXTSpeechRecognitionAssembly

- (EXTSpeechRecognitionViewController *)viewSpeechRecognition {
    return [TyphoonDefinition withClass:[EXTSpeechRecognitionViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSpeechRecognition]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterSpeechRecognition]];
                              [definition injectProperty:@selector(expenseService)
                                                    with:[self.globalAssembly expenseService]];
                              [definition injectProperty:@selector(keyboardControl)
                                                    with:[self.keyboardAssembly keyboardControl]];
                          }];
}

- (EXTSpeechRecognitionInteractor *)interactorSpeechRecognition {
    return [TyphoonDefinition withClass:[EXTSpeechRecognitionInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterSpeechRecognition]];
                          }];
}

- (EXTSpeechRecognitionPresenter *)presenterSpeechRecognition{
    return [TyphoonDefinition withClass:[EXTSpeechRecognitionPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewSpeechRecognition]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorSpeechRecognition]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerSpeechRecognition]];
                          }];
}

- (EXTSpeechRecognitionRouter *)routerSpeechRecognition{
    return [TyphoonDefinition withClass:[EXTSpeechRecognitionRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewSpeechRecognition]];
                          }];
}

@end
