//
//  EXTSpeechRecognitionAssembly.h
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author paul_arctic

 SpeechRecognition module
 */
@interface EXTSpeechRecognitionAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
