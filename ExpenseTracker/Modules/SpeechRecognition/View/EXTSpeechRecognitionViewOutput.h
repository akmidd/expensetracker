//
//  EXTSpeechRecognitionViewOutput.h
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TitleProtocol.h"

@protocol EXTSpeechRecognitionViewOutput <TitleProtocol>

/**
 @author paul_arctic

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;
- (void)showExpenses;

@end
