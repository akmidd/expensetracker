//
//  EXTSpeechRecognitionViewController.m
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTSpeechRecognitionViewController.h"
#import <Speech/Speech.h>
#import <AVFoundation/AVFoundation.h>
#import <QuartzCore/QuartzCore.h>

#import "EXTSpeechRecognitionViewOutput.h"
#import "EXTExpenseProtocol.h"
#import "EXTExpense.h"
#import "EXTExpenseCategory.h"

typedef NS_OPTIONS(NSUInteger, EXTExpenseState) {
    EXTExpenseStateNotStarted = 0,
    EXTExpenseStateAmountAsked,
    EXTExpenseStateAmountGot,
    EXTExpenseStateCategoryAsked,
    EXTExpenseStateCategoryGot,
    EXTExpenseStateFinished,
};

@interface EXTSpeechRecognitionViewController ()<SFSpeechRecognizerDelegate, AVAudioPlayerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, AVSpeechSynthesizerDelegate>

@property (strong,nonatomic) SFSpeechAudioBufferRecognitionRequest *recognitionRequest;
@property (strong,nonatomic) SFSpeechRecognitionTask *recognitionTask;
@property (strong,nonatomic) SFSpeechRecognizer *speechRecognizer;
@property (strong,nonatomic) AVAudioEngine *audioEngine;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIButton *speechButton;

@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic, assign) EXTExpenseState expenseState;

@property (nonatomic, strong) id<EXTExpenseProtocol> expense;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) BOOL needOneMoreRepeat;

@property (nonatomic, strong) SBTKeyboardControl *keyboardControl;
@property (nonatomic, strong) UIPickerView *categoryPicker;
@property (nonatomic, strong) NSArray<id<EXTExpenseCategoryProtocol>> * availableCategories;

@property (nonatomic, strong) AVSpeechSynthesizer *speechSynthesizer;

@end

@implementation EXTSpeechRecognitionViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    [self.output didTriggerViewReadyEvent];
    [self initialize];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.expenseState = EXTExpenseStateNotStarted;
    self.expense = nil;
    self.expense = [EXTExpense new];

    [self updateAvailableCategories];

    self.questionLabel.text = @"";
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self playQuestions];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self stopAnylize];
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self stopAnylize];
    [super viewDidDisappear:animated];
}

- (void)stopAnylize {
    self.expenseState = EXTExpenseStateNotStarted;
    self.expense = nil;
    [self deactivateTextView];
    [self killTimer];
    
    if (self.audioPlayer && self.audioPlayer.isPlaying) {
        [self.audioPlayer stop];
        self.audioPlayer = nil;
    }
    
    if (self.audioEngine.isRunning) {
        [self.audioEngine stop];
        [self.recognitionRequest endAudio];
        self.speechButton.enabled = NO;
        [self.speechButton setTitle:@"Начать запись" forState:UIControlStateNormal];
    }
}

- (void)configureSpeechSynthesizer {
    self.speechSynthesizer = [[AVSpeechSynthesizer alloc] init];
    self.speechSynthesizer.delegate = self;
}

#pragma mark - Методы EXTSpeechRecognitionViewInput

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
}

- (IBAction)showExpenses:(id)sender {
    [self.output showExpenses];
}

- (void)startSpeechButtonTapped {
    [self startSpeechButtonTapped:nil];
}

- (IBAction)startSpeechButtonTapped:(id)sender {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.audioEngine.isRunning) {
            [self.audioEngine stop];
            [self.recognitionRequest endAudio];
            self.speechButton.enabled = NO;
            [self.speechButton setTitle:@"Начать запись" forState:UIControlStateNormal];
        } else {
            [self startRecording];
            [self.speechButton setTitle:@"Остановить запись" forState:UIControlStateNormal];
        }
    });
}

- (void)initialize {
    self.title = [self.output title];
    self.expenseState = EXTExpenseStateNotStarted;
    [self configureTextView];
    [self configureSpeechRecognizer];
    [self configureSpeechSynthesizer];
}

- (void)configureTextView {
    self.textView.userInteractionEnabled = NO;
    self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.categoryPicker = [[UIPickerView alloc] init];
    self.categoryPicker.dataSource = self;
    self.categoryPicker.delegate = self;
    self.textView.inputView = self.categoryPicker;

    CGRect appFrame = [[UIScreen mainScreen] bounds];
    UIToolbar *inputSubmitAccessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0., 0., CGRectGetWidth(appFrame), 45.)];
    inputSubmitAccessoryView.items = @[[self flexSpace], [self categoryButtonItem]];
    self.textView.inputAccessoryView = inputSubmitAccessoryView;
}

- (UIBarButtonItem *)categoryButtonItem {
    UIButton *categoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    categoryButton.frame = CGRectMake(0, 0, 120, 20);
    [categoryButton.titleLabel setFont:[UIFont systemFontOfSize:13.]];
    [categoryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [categoryButton setTitle:NSLocalizedString(@"Выбрать", nil) forState:UIControlStateNormal];
    [categoryButton addTarget:self action:@selector(chooseCategory) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:categoryButton];
    return buttonItem;
}

- (void)chooseCategory {
    self.needOneMoreRepeat = YES;
    self.textView.text = self.expense.category.name;
    [self deactivateTextView];
}

- (UIBarButtonItem *)flexSpace {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                         target:nil
                                                         action:nil];
}

- (void)activateTextView {
    id<EXTExpenseCategoryProtocol> selectedCategory = _availableCategories.firstObject;
    self.expense.category = selectedCategory;
    [self.textView becomeFirstResponder];
}

- (void)deactivateTextView {
    [self.textView resignFirstResponder];
}

- (void)updateAvailableCategories {
    self.availableCategories = [_expenseService getAllExpenseCategories];
}

- (void)configureSpeechRecognizer {
    self.speechRecognizer = [[SFSpeechRecognizer alloc] initWithLocale:[NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
    self.audioEngine = [[AVAudioEngine alloc] init];
    self.speechRecognizer.delegate = self;
    
    self.speechButton.enabled = NO;
    
    self.speechButton.hidden = YES;
    
    [SFSpeechRecognizer requestAuthorization:^(SFSpeechRecognizerAuthorizationStatus status) {
        
        BOOL isButtonEnabled = NO;
        
        switch (status) {
            case SFSpeechRecognizerAuthorizationStatusAuthorized:
            isButtonEnabled = YES;
            break;
            case SFSpeechRecognizerAuthorizationStatusDenied:
            isButtonEnabled = NO;
            break;
            case SFSpeechRecognizerAuthorizationStatusRestricted:
            isButtonEnabled = NO;
            break;
            case SFSpeechRecognizerAuthorizationStatusNotDetermined:
            isButtonEnabled = NO;
            break;
        }
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            self.speechButton.enabled = isButtonEnabled;
        }];
    }];
}

- (void)startRecording {
    if (self.recognitionTask != nil) {
        [self.recognitionTask cancel];
        self.recognitionTask = nil;
    }
    
    [self audioRecorderSession];
    
    self.recognitionRequest = [SFSpeechAudioBufferRecognitionRequest new];
    self.recognitionRequest.shouldReportPartialResults = YES;
    
    self.recognitionTask = [self.speechRecognizer recognitionTaskWithRequest:self.recognitionRequest resultHandler:^(SFSpeechRecognitionResult * _Nullable result, NSError * _Nullable error) {
        BOOL isFinal = NO;
        if (result != nil) {
            self.textView.text = [self textForUserAnswerWithRecognizedText:result.bestTranscription.formattedString];
//            self.textView.text = result.bestTranscription.formattedString;
            [self updateRepeateFlag];
            isFinal = result.isFinal;
        }
        
        if (error != nil || isFinal) {
            [self getAnswers];
            self.textView.text = @"";
            [self.audioEngine stop];
            [self.audioEngine.inputNode removeTapOnBus:0];
            self.recognitionRequest = nil;
            self.recognitionTask = nil;
            self.speechButton.enabled = true;
        }
    }];
    
    [self.audioEngine.inputNode installTapOnBus:0 bufferSize:1024 format:[self.audioEngine.inputNode outputFormatForBus:0] block:^(AVAudioPCMBuffer * _Nonnull buffer, AVAudioTime * _Nonnull when) {
        [self.recognitionRequest appendAudioPCMBuffer:buffer];
    }];
    
    [self.audioEngine prepare];
    [self.audioEngine startAndReturnError:nil];
    
    [self updateTimer];
}

- (void)updateTimer {
    [self killTimer];
    NSTimeInterval time = [self timeout];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(checkUserAnswer) userInfo:nil repeats:YES];
}

- (NSTimeInterval)timeout {
    if (self.expenseState == EXTExpenseStateAmountAsked) {
        return .5;
    }
    else if (self.expenseState == EXTExpenseStateCategoryAsked) {
        return .3;
    }
    else if (self.expenseState == EXTExpenseStateFinished) {
        return .0;
    }
    return .0;
}

- (void)checkUserAnswer {
    if (self.needOneMoreRepeat) {
        self.needOneMoreRepeat = NO;
        return ;
    }
    
    if (self.expenseState == EXTExpenseStateAmountAsked && [self isAmountValid]) {
        [self killTimer];
        [self startSpeechButtonTapped];
    }
    else if (self.expenseState == EXTExpenseStateCategoryAsked && [self isCategoryValid]) {
        [self killTimer];
        [self startSpeechButtonTapped];
    }
    else if (self.expenseState == EXTExpenseStateFinished) {
        [self killTimer];
        [self startSpeechButtonTapped];
    }
}

- (NSString*)textForUserAnswerWithRecognizedText:(NSString*)recognizedText {
    NSString *text;
    if (self.expenseState == EXTExpenseStateAmountAsked) {
        text = @"";
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        
        NSArray * words = [recognizedText componentsSeparatedByString:@" "];
        for (NSString *word in words) {
            NSNumber *amount = [f numberFromString:word];
            if (amount) {
                NSUInteger index = [words indexOfObject:word];
                if (index != 0) {
                    NSMutableArray *newWords = [NSMutableArray array];
                    for (NSUInteger i = index; i < words.count; i++) {
                        [newWords addObject:words[i]];
                    }
                    text = [newWords componentsJoinedByString:@" "];
                }
                else {
                    text = recognizedText;
                }
                break;
            }
        }
    }
    else if (self.expenseState == EXTExpenseStateCategoryAsked) {
        text = recognizedText;
    }
    else if (self.expenseState == EXTExpenseStateFinished) {
        text = recognizedText;
    }
    return text;
}

- (BOOL)isAmountValid {
    BOOL flag = FALSE;
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    NSArray * words = [[self.textView.text copy] componentsSeparatedByString:@" "];
    for (NSString *word in words) {
        NSNumber *amount = [f numberFromString:word];
        if (amount) {
            flag = TRUE;
            break;
        }
    }
    
    return flag;
}

- (BOOL)isCategoryValid {
    return (self.textView.text && self.textView.text.length > 0) ? TRUE : FALSE;
}

- (void)killTimer {
    if (self.timer && self.timer.isValid) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)updateRepeateFlag {
    if (self.expenseState == EXTExpenseStateAmountAsked) {
        self.needOneMoreRepeat = YES;
    }
    else if (self.expenseState == EXTExpenseStateCategoryAsked) {
        self.needOneMoreRepeat = YES;
    }
    else if (self.expenseState == EXTExpenseStateFinished) {
        self.needOneMoreRepeat = NO;
    }
}

- (AVAudioSession*)audioPlayerSession {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setMode:AVAudioSessionModeDefault error:nil];
    [audioSession setActive: YES error: nil];
    return audioSession;
}

- (AVAudioSession*)audioRecorderSession {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryRecord error:nil];
    [audioSession setMode:AVAudioSessionModeMeasurement error:nil];
    [audioSession setActive:YES withOptions:AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation error:nil];
    return audioSession;
}

- (void)playQuestions {
    if (self.expenseState == EXTExpenseStateNotStarted) {
        [self playAmount];
    }
    else if (self.expenseState == EXTExpenseStateAmountGot) {
        [self playCategory];
    }
    else if (self.expenseState == EXTExpenseStateCategoryGot) {
        [self playSuccess];
    }
}

//- (void)playAmount {
//    self.expenseState = EXTExpenseStateAmountAsked;
//    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Amount"  ofType:@"m4a"];
//    [self playAudioWithResource:soundFilePath];
//}
//
//- (void)playCategory {
//    self.expenseState = EXTExpenseStateCategoryAsked;
//    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Category"  ofType:@"m4a"];
//    [self playAudioWithResource:soundFilePath];
//    [self activateTextView];
//}
//
//- (void)playSuccess {
//    self.expenseState = EXTExpenseStateFinished;
//    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"Success"  ofType:@"m4a"];
//    [self playAudioWithResource:soundFilePath];
//}
//
//- (void)playAudioWithResource:(NSString*)filePath {
//    if (self.audioPlayer && self.audioPlayer.isPlaying) {
//        [self.audioPlayer stop];
//        self.audioPlayer = nil;
//    }
//
//    NSURL *soundFileURL = [NSURL fileURLWithPath:filePath];
//    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
//
//    [self audioPlayerSession];
//
//    [self.audioPlayer setVolume:1.0];
//    self.audioPlayer.delegate = self;
//
//    [self.audioPlayer setCurrentTime:0];
//    [self.audioPlayer play];
//
//    [self showDescription];
//}

- (void)playAmount {
    self.expenseState = EXTExpenseStateAmountAsked;
    [self playAudioWithText:[self currentTextDescription]];
}

- (void)playCategory {
    self.expenseState = EXTExpenseStateCategoryAsked;
    [self playAudioWithText:[self currentTextDescription]];
    [self activateTextView];
}

- (void)playSuccess {
    self.expenseState = EXTExpenseStateFinished;
    [self playAudioWithText:[self currentTextDescription]];
}

- (void)playAudioWithText:(NSString*)text {
    if (self.speechSynthesizer && (self.speechSynthesizer.isSpeaking || self.speechSynthesizer.isPaused)) {
        self.speechSynthesizer = nil;
        [self configureSpeechSynthesizer];
    }
    
    [self audioPlayerSession];
    
    AVSpeechUtterance *speechutt = [AVSpeechUtterance speechUtteranceWithString:text];
    [speechutt setRate:.5f];
    speechutt.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"ru-Ru"];
    [self.speechSynthesizer speakUtterance:speechutt];
    
    [self showDescription];
}

- (void)prepareForGetAnswers {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.expenseState == EXTExpenseStateAmountAsked) {
            [self startSpeechButtonTapped];
        }
        else if (self.expenseState == EXTExpenseStateCategoryAsked) {
            [self startSpeechButtonTapped];
        }
        else if (self.expenseState == EXTExpenseStateFinished) {
            [self startSpeechButtonTapped];
        }
    });
}

- (void)getAnswers {
    if (self.expenseState == EXTExpenseStateAmountAsked) {
        [self anylizeUserAnswer];
        self.expenseState = EXTExpenseStateAmountGot;
    }
    else if (self.expenseState == EXTExpenseStateCategoryAsked) {
        [self anylizeUserAnswer];
        self.expenseState = EXTExpenseStateCategoryGot;
    }
    else if (self.expenseState == EXTExpenseStateFinished) {
        [self anylizeUserAnswer];
        self.expenseState = EXTExpenseStateNotStarted;
        [self.output showExpenses];
        return ;
    }
    [self playQuestions];
}

- (void)showDescription {
    dispatch_async(dispatch_get_main_queue(), ^{
        self.questionLabel.text = [self currentTextDescription];
    });
}

- (NSString*)currentTextDescription {
    NSString *text;
    if (self.expenseState == EXTExpenseStateAmountAsked) {
        text = @"Назовите сумму.";
    }
    else if (self.expenseState == EXTExpenseStateCategoryAsked) {
        text = @"Назовите или выберите категорию затрат.";
    }
    else if (self.expenseState == EXTExpenseStateFinished) {
        text = [NSString stringWithFormat:@"Операция выполнена: добавлена запись суммой %@ руб. на категорию затрат %@", self.expense.amount, self.expense.category.name];
    }
    else {
        text = @"";
    }
    return text;
}

#pragma mark - <AVAudioPlayerDelegate>

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    if (self.audioPlayer && self.audioPlayer.isPlaying) {
        [self.audioPlayer stop];
        self.audioPlayer = nil;
    }
    
    [self prepareForGetAnswers];
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError * __nullable)error {
    if (self.audioPlayer && self.audioPlayer.isPlaying) {
        [self.audioPlayer stop];
        self.audioPlayer = nil;
    }
    NSLog(@"audioPlayerDecodeErrorDidOccur");
}


#pragma mark - <SFSpeechRecognizerDelegate>

- (void)speechRecognizer:(SFSpeechRecognizer *)speechRecognizer availabilityDidChange:(BOOL)available {
    if (available) {
        self.speechButton.enabled = YES;
    } else {
        self.speechButton.enabled = NO;
    }
}

- (void)anylizeUserAnswer {
    if (self.expenseState == EXTExpenseStateAmountAsked) {
        [self extractAmount];
    }
    else if (self.expenseState == EXTExpenseStateCategoryAsked) {
        [self extractCategory];
    }
    else if (self.expenseState == EXTExpenseStateFinished) {
        [self extractData];
    }
}

- (void)extractAmount {
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    NSArray * words = [[self.textView.text copy] componentsSeparatedByString:@" "];
    for (NSString *word in words) {
        NSNumber *amount = [f numberFromString:word];
        if (amount) {
            NSNumber *myNumber = [f numberFromString:word];
            NSDecimalNumber *expAmount = [NSDecimalNumber decimalNumberWithDecimal:[myNumber decimalValue]];
            [self.expense setAmount:expAmount];
            break;
        }
    }
}

- (void)extractCategory {
    NSString *categoryName = [self.textView.text copy];
    EXTExpenseCategory *category = [self.expenseService createNewCategoryWithName:categoryName];
    [self.expense setCategory:category];
    [self deactivateTextView];
}

- (void)extractData {
    [self.expenseService saveExpense:self.expense];
}


#pragma - mark <UIPickerViewDelegate & UIPickerViewDataSource> Methods

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_availableCategories count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    id<EXTExpenseCategoryProtocol> category = _availableCategories[row];
    return category.name;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    id<EXTExpenseCategoryProtocol> selectedCategory = _availableCategories[row];
    self.expense.category = selectedCategory;
}

//- (void)configureSpeechSynthesizer {
//    NSString *str = @"Привет, как дела?";
//
//    [self audioPlayerSession];
//
//    self.speechSynthesizer = [[AVSpeechSynthesizer alloc] init];
//    self.speechSynthesizer.delegate = self;
//    AVSpeechUtterance *speechutt = [AVSpeechUtterance speechUtteranceWithString:str];
//    [speechutt setRate:.5f];
//    speechutt.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"ru-Ru"];
//    [self.speechSynthesizer speakUtterance:speechutt];
//}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didStartSpeechUtterance:(AVSpeechUtterance *)utterance {
//    NSLog(@"START");
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance {
    if (self.speechSynthesizer && (self.speechSynthesizer.isSpeaking || self.speechSynthesizer.isPaused)) {
        self.speechSynthesizer = nil;
        [self configureSpeechSynthesizer];
    }

    [self prepareForGetAnswers];
}
//- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didPauseSpeechUtterance:(AVSpeechUtterance *)utterance;
//- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didContinueSpeechUtterance:(AVSpeechUtterance *)utterance;
//- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didCancelSpeechUtterance:(AVSpeechUtterance *)utterance;

@end
