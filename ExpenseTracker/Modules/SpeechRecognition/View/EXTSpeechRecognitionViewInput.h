//
//  EXTSpeechRecognitionViewInput.h
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EXTSpeechRecognitionViewInput <NSObject>

/**
 @author paul_arctic

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

@end
