//
//  EXTSpeechRecognitionViewController.h
//  ExpenseTracker
//
//  Created by paul_arctic on 07/04/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EXTSpeechRecognitionViewInput.h"
#import "EXTExpenseServiceProtocol.h"
#import "SBTKeyboardControl.h"

@protocol EXTSpeechRecognitionViewOutput;

@interface EXTSpeechRecognitionViewController : UIViewController <EXTSpeechRecognitionViewInput>

@property (nonatomic, strong) id<EXTSpeechRecognitionViewOutput> output;
@property(nonatomic, strong) id<EXTExpenseServiceProtocol> expenseService;
@property (nonatomic, strong, readonly) SBTKeyboardControl *keyboardControl;

@end
