//
//  EXTCategoryModuleOutput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>
#import "EXTExpenseCategory.h"

@protocol EXTCategoryModuleOutput<RamblerViperModuleOutput>

-(void) didCreateNewCategory:(id<EXTExpenseCategoryProtocol>)category;

@end
