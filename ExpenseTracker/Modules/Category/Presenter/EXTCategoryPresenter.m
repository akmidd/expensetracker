//
//  EXTCategoryPresenter.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTCategoryPresenter.h"

#import "EXTCategoryViewInput.h"
#import "EXTCategoryInteractorInput.h"
#import "EXTCategoryRouterInput.h"
#import "EXTExpenseCategory.h"

@implementation EXTCategoryPresenter

#pragma mark - Методы EXTCategoryModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

#pragma mark - Методы EXTCategoryViewOutput

- (void)didTriggerViewReadyEvent {
	[self.view setupInitialState];
}


- (NSString *)itemTitle {
    return [self.interactor itemTitle];
}

- (NSString *)title {
    return [self.interactor title];
}

- (void)onRightBarButtonItemTap {
    id<EXTExpenseCategoryProtocol> category = [_expenseService createNewCategoryWithName:self.view.categoryName];
    [self.output didCreateNewCategory:category];
    [self.router closeModuleFromViewController:(UIViewController*)self.view];
}

#pragma mark - Методы EXTCategoryInteractorOutput

@end
