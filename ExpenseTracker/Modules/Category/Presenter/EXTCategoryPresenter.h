//
//  EXTCategoryPresenter.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTCategoryViewOutput.h"
#import "EXTCategoryInteractorOutput.h"
#import "EXTCategoryModuleInput.h"
#import "EXTExpenseServiceProtocol.h"
#import "EXTCategoryModuleOutput.h"

@protocol EXTCategoryViewInput;
@protocol EXTCategoryInteractorInput;
@protocol EXTCategoryRouterInput;

@interface EXTCategoryPresenter : NSObject <EXTCategoryModuleInput, EXTCategoryViewOutput, EXTCategoryInteractorOutput>

@property (nonatomic, weak) id<EXTCategoryViewInput> view;
@property (nonatomic, strong) id<EXTCategoryInteractorInput> interactor;
@property (nonatomic, strong) id<EXTCategoryRouterInput> router;
@property (nonatomic, strong) id<EXTCategoryModuleOutput> output;

@property(nonatomic, strong) id<EXTExpenseServiceProtocol> expenseService;

@end
