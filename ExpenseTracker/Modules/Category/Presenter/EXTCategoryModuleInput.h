//
//  EXTCategoryModuleInput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol EXTCategoryModuleInput <RamblerViperModuleInput>

/**
 @author sbt-klochkov-dv

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@end
