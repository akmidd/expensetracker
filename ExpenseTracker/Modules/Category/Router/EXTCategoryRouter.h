//
//  EXTCategoryRouter.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTCategoryRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface EXTCategoryRouter : NSObject <EXTCategoryRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
