//
//  EXTCategoryRouterInput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EXTCategoryRouterInput <NSObject>

-(void)closeModuleFromViewController:(UIViewController *) viewController;

@end
