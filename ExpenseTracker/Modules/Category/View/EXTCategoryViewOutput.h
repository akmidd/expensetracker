//
//  EXTCategoryViewOutput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXTExpenseNavigationItemProtocol.h"
#import "TitleProtocol.h"

@protocol EXTCategoryViewOutput <EXTExpenseNavigationItemProtocol, TitleProtocol>

/**
 @author sbt-klochkov-dv

 Метод сообщает презентеру о том, что view готова к работе
 */
- (void)didTriggerViewReadyEvent;

@end
