//
//  EXTCategoryViewInput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TitleProtocol.h"

@protocol EXTCategoryViewInput <NSObject, TitleProtocol>

@property (nonatomic, readonly) NSString * categoryName;

/**
 @author sbt-klochkov-dv

 Метод настраивает начальный стейт view
 */
- (void)setupInitialState;

@end
