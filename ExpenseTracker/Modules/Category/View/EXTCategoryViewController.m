//
//  EXTCategoryViewController.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTCategoryViewController.h"

#import "EXTCategoryViewOutput.h"
#import "SBTTextField.h"
#import "UIViewController+TapToHideKeyboard.h"


@interface EXTCategoryViewController ()
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet SBTTextField *categoryNameTextField;

@end

@implementation EXTCategoryViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    
    [self setupRightBarButtonItem];
    _saveButton.layer.cornerRadius = CGRectGetHeight(_saveButton.frame) / 2;
    
    self.title = self.output.title;
    self.view.hidesKeyboardOnTap = YES;
	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы EXTCategoryViewInput

- (void)setupInitialState {
    self.categoryNameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
}


- (void)setupRightBarButtonItem {
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:[self.output itemTitle] style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonDidTap:)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

-(NSString *)categoryName {
    return _categoryNameTextField.text;
}

- (IBAction) doneButtonDidTap:(id)sender {
    [self.output onRightBarButtonItemTap];
}

@end
