//
//  EXTCategoryViewController.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EXTCategoryViewInput.h"

@protocol EXTCategoryViewOutput;

@interface EXTCategoryViewController : UIViewController <EXTCategoryViewInput>

@property (nonatomic, strong) id<EXTCategoryViewOutput> output;

@end
