//
//  EXTCategoryAssembly.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "EXTAsembly.h"

/**
 @author sbt-klochkov-dv

 Category module
 */
@interface EXTCategoryAssembly : TyphoonAssembly <RamblerInitialAssembly>
@property(nonatomic, strong) EXTAsembly * globalAssembly;
@end
