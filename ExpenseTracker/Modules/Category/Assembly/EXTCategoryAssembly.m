//
//  EXTCategoryAssembly.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTCategoryAssembly.h"

#import "EXTCategoryViewController.h"
#import "EXTCategoryInteractor.h"
#import "EXTCategoryPresenter.h"
#import "EXTCategoryRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation EXTCategoryAssembly

- (EXTCategoryViewController *)viewCategory {
    return [TyphoonDefinition withClass:[EXTCategoryViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCategory]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterCategory]];
                          }];
}

- (EXTCategoryInteractor *)interactorCategory {
    return [TyphoonDefinition withClass:[EXTCategoryInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterCategory]];
                          }];
}

- (EXTCategoryPresenter *)presenterCategory{
    return [TyphoonDefinition withClass:[EXTCategoryPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewCategory]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorCategory]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerCategory]];
                              [definition injectProperty:@selector(expenseService) with:[self.globalAssembly expenseService]];
                          }];
}

- (EXTCategoryRouter *)routerCategory{
    return [TyphoonDefinition withClass:[EXTCategoryRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewCategory]];
                          }];
}

@end
