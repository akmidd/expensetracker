//
//  EXTCategoryInteractor.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTCategoryInteractor.h"

#import "EXTCategoryInteractorOutput.h"

@implementation EXTCategoryInteractor

#pragma mark - Методы EXTCategoryInteractorInput

- (NSString *)itemTitle {
    return @"Создать";
}


- (NSString *)title {
    //Здесь нужно добавить локализацию
    return @"Новая категория";
}

@end
