//
//  EXTCategoryInteractorInput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "EXTExpenseNavigationItemProtocol.h"
#import "TitleProtocol.h"

@protocol EXTCategoryInteractorInput <EXTExpenseNavigationItemProtocol, TitleProtocol>

@end
