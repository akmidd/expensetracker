//
//  EXTCategoryInteractor.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 28/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTCategoryInteractorInput.h"

@protocol EXTCategoryInteractorOutput;

@interface EXTCategoryInteractor : NSObject <EXTCategoryInteractorInput>

@property (nonatomic, weak) id<EXTCategoryInteractorOutput> output;

@end
