//
//  EXTExpenseDetailsEditPresenter.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpenseDetailsEditPresenter.h"

#import "EXTExpenseDetailsEditViewInput.h"
#import "EXTExpenseDetailsEditInteractorInput.h"
#import "EXTExpenseDetailsEditRouterInput.h"

@interface EXTExpenseDetailsEditPresenter()

@property (nonatomic, strong) id<EXTExpenseProtocol> expense;

@end

@implementation EXTExpenseDetailsEditPresenter
@synthesize expenseUuid = _expenseUuid;

#pragma mark - Методы EXTExpenseDetailsEditModuleInput

- (void)configureModule {
    // Стартовая конфигурация модуля, не привязанная к состоянию view
}

-(void)setExpenseUuid:(NSUUID *)expenseUuid {
    _expenseUuid = expenseUuid;
    [self updateView];
}


#pragma mark - Методы EXTExpenseDetailsEditViewOutput

- (void)didTriggerViewReadyEvent {
    if (_expenseUuid) {
        _expense = [_expenseService getExpenseByUuid:_expenseUuid];
    }
    
    if (!_expense) {
        _expense = [[EXTExpense alloc] init];
        _expense.category = [_expenseService getAllExpenseCategories][0];
    }
    
    self.view.expense = _expense;
	[self.view setupInitialState];
}

-(void)didTriggerViewWillAppearEvent {
    [self updateView];
}

-(void)newCategoryButtonTapped {
    [self.router showNewCategoryViewFromViewController:(UIViewController *) self.view];
}

-(void)didCreateNewCategory:(id<EXTExpenseCategoryProtocol>)category {
    _expense.category = category;
    [self.view update];
}

-(void) createNewCategoryWithName:(NSString *) name {
    [_expenseService createNewCategoryWithName:name];
    [self updateView];
}

-(void) ammoutValueDidSet:(NSDecimalNumber *) ammount {
    _expense.amount = ammount;
}

- (void)expenseCommentChanged:(NSString *)comment {
    _expense.note = comment;
}

-(void)expenseCategoryDidSet:(id<EXTExpenseCategoryProtocol>) category {
    _expense.category = category;
}

-(void)updateView {
    self.view.availableCategories = [_expenseService getAllExpenseCategories];
    [self.view update];
}

- (NSString *)itemTitle {
    return [self.interactor itemTitle];
}

- (NSString *)title {
    return [self.interactor title];
}

- (void)onRightBarButtonItemTap {
    [_expenseService saveExpense:_expense];
    [self.router closeModuleFromViewController:(UIViewController*)self.view];
}

#pragma mark - Методы EXTExpenseDetailsEditInteractorOutput

@end
