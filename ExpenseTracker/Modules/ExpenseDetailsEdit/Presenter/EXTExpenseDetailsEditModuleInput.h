//
//  EXTExpenseDetailsEditModuleInput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol EXTExpenseDetailsEditModuleInput <RamblerViperModuleInput>

@property(nonatomic, strong) NSUUID * expenseUuid;

/**
 @author sbt-klochkov-dv

 Метод инициирует стартовую конфигурацию текущего модуля
 */
- (void)configureModule;

@end
