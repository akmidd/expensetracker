//
//  EXTExpenseDetailsEditPresenter.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpenseDetailsEditViewOutput.h"
#import "EXTExpenseDetailsEditInteractorOutput.h"
#import "EXTExpenseDetailsEditModuleInput.h"
#import "EXTExpenseServiceProtocol.h"
#import "EXTCategoryModuleOutput.h"

@protocol EXTExpenseDetailsEditViewInput;
@protocol EXTExpenseDetailsEditInteractorInput;
@protocol EXTExpenseDetailsEditRouterInput;

@interface EXTExpenseDetailsEditPresenter : NSObject <EXTExpenseDetailsEditModuleInput, EXTExpenseDetailsEditViewOutput, EXTExpenseDetailsEditInteractorOutput, EXTCategoryModuleOutput>

@property (nonatomic, weak) id<EXTExpenseDetailsEditViewInput> view;
@property (nonatomic, strong) id<EXTExpenseDetailsEditInteractorInput> interactor;
@property (nonatomic, strong) id<EXTExpenseDetailsEditRouterInput> router;

@property(nonatomic, strong) id<EXTExpenseServiceProtocol> expenseService;

@end
