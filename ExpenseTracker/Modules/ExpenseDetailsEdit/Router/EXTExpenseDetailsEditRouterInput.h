//
//  EXTExpenseDetailsEditRouterInput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EXTExpenseDetailsEditRouterInput <NSObject>

@required
- (void)closeModuleFromViewController:(UIViewController*)viewController;
- (void) showNewCategoryViewFromViewController:(UIViewController*)viewController;

@end
