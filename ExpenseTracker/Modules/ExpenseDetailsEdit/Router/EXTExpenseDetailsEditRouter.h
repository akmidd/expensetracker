//
//  EXTExpenseDetailsEditRouter.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpenseDetailsEditRouterInput.h"
#import "EXTExpenseDetailsEditPresenter.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface EXTExpenseDetailsEditRouter : NSObject <EXTExpenseDetailsEditRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@property (nonatomic, weak) EXTExpenseDetailsEditPresenter * presenter;

@end
