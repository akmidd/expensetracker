//
//  EXTExpenseDetailsEditRouter.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpenseDetailsEditRouter.h"

#import <ViperMcFlurry/ViperMcFlurry.h>
#import "EXTCategoryViewController.h"
#import "EXTCategoryPresenter.h"

@implementation EXTExpenseDetailsEditRouter

#pragma mark - Методы EXTExpenseDetailsEditRouterInput

- (void)closeModuleFromViewController:(UIViewController*)viewController {
    [viewController.navigationController popViewControllerAnimated:YES];
}

-(void) showNewCategoryViewFromViewController:(UIViewController *)viewController {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Category" bundle:[NSBundle bundleForClass:[self class]]];
    
    EXTCategoryViewController *categoryViewController = (EXTCategoryViewController *) [storyboard instantiateInitialViewController];
    
    [(EXTCategoryPresenter * )categoryViewController.output setOutput:self.presenter];
    
    [viewController.navigationController pushViewController:categoryViewController animated:YES];
    
}

@end
