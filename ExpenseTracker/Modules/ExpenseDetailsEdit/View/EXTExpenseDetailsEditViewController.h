//
//  EXTExpenseDetailsEditViewController.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EXTExpenseDetailsEditViewInput.h"

@protocol EXTExpenseDetailsEditViewOutput;
@class SBTKeyboardControl;

@interface EXTExpenseDetailsEditViewController : UIViewController <EXTExpenseDetailsEditViewInput>

@property (nonatomic, strong) id<EXTExpenseDetailsEditViewOutput> output;
@property (nonatomic, strong, readonly) SBTKeyboardControl *keyboardControl;

@end
