//
//  EXTExpenseDetailsEditViewOutput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXTExpenseCategory.h"
#import "EXTExpenseNavigationItemProtocol.h"
#import "TitleProtocol.h"

@protocol EXTExpenseDetailsEditViewOutput <EXTExpenseNavigationItemProtocol, TitleProtocol>

/**
 @author sbt-klochkov-dv

 Метод сообщает презентеру о том, что view готова к работе
 */

@required
- (void)didTriggerViewReadyEvent;
- (void)didTriggerViewWillAppearEvent;
-(void) ammoutValueDidSet:(NSDecimalNumber *) ammount;
-(void) expenseCommentChanged:(NSString *) comment;
-(void) expenseCategoryDidSet:(id<EXTExpenseCategoryProtocol>) category;
-(void) newCategoryButtonTapped;
-(void) createNewCategoryWithName:(NSString *) name;

@end
