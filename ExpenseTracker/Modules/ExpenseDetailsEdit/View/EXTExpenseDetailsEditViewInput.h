//
//  EXTExpenseDetailsEditViewInput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXTExpense.h"

@protocol EXTExpenseDetailsEditViewInput <NSObject>


@property(nonatomic, strong) id<EXTExpenseProtocol> expense;
@property(nonatomic, strong) NSArray<id<EXTExpenseCategoryProtocol>> * availableCategories;

/**
 @author sbt-klochkov-dv

 Метод настраивает начальный стейт view
 */
@required
- (void)setupInitialState;
- (void)loadExpenseWithUuid:(NSUUID *) expenseUuid;
- (void)update;

@end
