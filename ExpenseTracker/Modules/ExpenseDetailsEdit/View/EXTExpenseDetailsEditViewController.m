//
//  EXTExpenseDetailsEditViewController.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpenseDetailsEditViewController.h"
#import "EXTExpenseDetailsEditViewOutput.h"
#import "UIViewController+TapToHideKeyboard.h"
#import "SBTKeyboardControl.h"
#import "SBTKeyboardControlDelegate.h"

@interface EXTExpenseDetailsEditViewController() <UIPickerViewDelegate, UIPickerViewDataSource, SBTKeyboardControlDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *wrapperView;
@property (weak, nonatomic) IBOutlet UITextField *amountTextField;
@property (weak, nonatomic) IBOutlet UITextField *commentTextField;
@property (weak, nonatomic) IBOutlet UITextField *categoryPickerTextField;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (strong, nonatomic) IBOutlet UIPickerView *categoryPicker;

@property (nonatomic, strong) SBTKeyboardControl *keyboardControl;

@end

@implementation EXTExpenseDetailsEditViewController

@synthesize expense = _expense;
@synthesize availableCategories = _availableCategories;

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad {
	[super viewDidLoad];
    
    [self setupRightBarButtonItem];
    
    self.amountTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.amountTextField.keyboardType = UIKeyboardTypeDecimalPad;
    
    self.commentTextField.autocorrectionType = UITextAutocorrectionTypeNo;

    self.categoryPicker = [[UIPickerView alloc] init];
    self.categoryPicker.dataSource = self;
    self.categoryPicker.delegate = self;
    
    self.doneButton.layer.cornerRadius = CGRectGetHeight(self.doneButton.frame) / 2;
    [self.doneButton setTitle:self.output.itemTitle forState:UIControlStateNormal];
    
    self.categoryPickerTextField.inputView = self.categoryPicker;
    
    CGRect appFrame = [[UIScreen mainScreen] bounds];
    UIToolbar *inputSubmitAccessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0., 0., CGRectGetWidth(appFrame), 45.)];
    inputSubmitAccessoryView.items = @[[self chooseButtonItem], [self flexSpace], [self categoryButtonItem]];
    self.categoryPickerTextField.inputAccessoryView = inputSubmitAccessoryView;
    
    self.view.hidesKeyboardOnTap = YES;
 
	[self.output didTriggerViewReadyEvent];
    
    [self.keyboardControl updateWithInputFields:@[self.amountTextField, self.commentTextField] delegate:self scrollView:self.scrollView];
    [self.keyboardControl setSubmitButtonTitle:NSLocalizedString(@"Продолжить",nil)];
    [self.keyboardControl setSubmitButtonHidden:self.amountTextField.text.length > 0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.view triggerHideKeyboard];
    [super viewWillDisappear:animated];
}

- (void)setupRightBarButtonItem {
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:[self.output itemTitle] style:UIBarButtonItemStyleDone target:self action:@selector(doneButtonDidTap:)];
    self.navigationItem.rightBarButtonItem = rightItem;
}

#pragma mark - Actions

- (IBAction)doneButtonDidTap:(id)sender {
    [self.output onRightBarButtonItemTap];
}

- (void)keyboardSubmitButtonTapped {
    [self doneButtonDidTap:nil];
}

#pragma mark - Методы EXTExpenseDetailsEditViewInput

- (void)viewWillAppear:(BOOL)animated {
    [self.output didTriggerViewWillAppearEvent];
}

- (void)setupInitialState {
	// В этом методе происходит настройка параметров view, зависящих от ее жизненого цикла (создание элементов, анимации и пр.)
}

- (IBAction)newCategoryButtonTapped:(id)sender {
    [self.output newCategoryButtonTapped];
}

- (NSInteger)numberOfComponentsInPickerView:(nonnull UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(nonnull UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [_availableCategories count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    id<EXTExpenseCategoryProtocol> category = _availableCategories[row];
    return category.name;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    id<EXTExpenseCategoryProtocol> selectedCategory = _availableCategories[row];
    [self.categoryButton setTitle:selectedCategory.name forState:UIControlStateNormal];
    self.title = selectedCategory.name;
    [self.output expenseCategoryDidSet:selectedCategory];
}

-(void) update {
    _amountTextField.text = [_expense.amount stringValue];
    if (_availableCategories && _availableCategories.count > 0) {
        [_categoryPicker reloadAllComponents];
        NSUInteger categoryIndex = [_availableCategories indexOfObjectPassingTest:^BOOL(id<EXTExpenseCategoryProtocol>  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            return [obj.uuid isEqual:_expense.category.uuid];
        }];
        
        if (categoryIndex != NSNotFound) {
            [self.categoryPicker selectRow:categoryIndex inComponent:0 animated:true];
        }
    }
    NSString *categoryName = NSLocalizedString(@"Выберете категорию", nil);
    if (self.expense.category.name.length > 0) {
        categoryName = self.expense.category.name;
    }
    self.commentTextField.text = _expense.note;
    [self.categoryButton setTitle:categoryName forState:UIControlStateNormal];
    self.title = self.output.title;
}

- (IBAction)amountChanged:(id)sender {
    NSString * amountStr = _amountTextField.text;
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *number = [f numberFromString:amountStr];
    NSDecimalNumber *amount = [NSDecimalNumber decimalNumberWithDecimal:[number decimalValue]];
    [self.output ammoutValueDidSet:amount];
}

- (IBAction)commentTextFieldChanged:(id)sender {
    [self.output expenseCommentChanged:self.commentTextField.text];
}


#pragma mark - Actions

- (IBAction)categotyButtonDidTap:(UIButton *)sender {
    [self.categoryPickerTextField becomeFirstResponder];
}

- (UIBarButtonItem *)categoryButtonItem {
    UIButton *categoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    categoryButton.frame = CGRectMake(0, 0, 120, 20);
    [categoryButton.titleLabel setFont:[UIFont systemFontOfSize:13.]];
    [categoryButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [categoryButton setTitle:NSLocalizedString(@"Новая категория", nil) forState:UIControlStateNormal];
    [categoryButton addTarget:self action:@selector(newCategoryButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:categoryButton];
    return buttonItem;
}

- (UIBarButtonItem *)flexSpace {
    return [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                         target:nil
                                                         action:nil];
}

- (UIBarButtonItem *)chooseButtonItem {
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(0, 0, 60, 20);
    [closeButton.titleLabel setFont:[UIFont systemFontOfSize:13.]];
    [closeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [closeButton setTitle:NSLocalizedString(@"Выбрать", nil) forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(hideCategoriesChoose) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:closeButton];
}

- (void)hideCategoriesChoose {
    [self.categoryPickerTextField resignFirstResponder];
}

@end
