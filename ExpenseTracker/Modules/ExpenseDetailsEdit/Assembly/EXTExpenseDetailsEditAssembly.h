//
//  EXTExpenseDetailsEditAssembly.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "EXTAsembly.h"

/**
 @author sbt-klochkov-dv

 ExpenseDetailsEdit module
 */
@interface EXTExpenseDetailsEditAssembly : TyphoonAssembly <RamblerInitialAssembly>
@property(nonatomic, strong) EXTAsembly * globalAssembly;
@end
