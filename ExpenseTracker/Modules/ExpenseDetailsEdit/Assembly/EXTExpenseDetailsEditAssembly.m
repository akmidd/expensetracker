//
//  EXTExpenseDetailsEditAssembly.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpenseDetailsEditAssembly.h"

#import "EXTExpenseDetailsEditViewController.h"
#import "EXTExpenseDetailsEditInteractor.h"
#import "EXTExpenseDetailsCreateInteractor.h"
#import "EXTExpenseDetailsEditPresenter.h"
#import "EXTExpenseDetailsEditRouter.h"
#import "SBTKeyboardControlAssembly.h"

#import <ViperMcFlurry/ViperMcFlurry.h>


@interface EXTExpenseDetailsEditAssembly()

@property (nonatomic, weak) SBTKeyboardControlAssembly *keyboardAssembly;

@end

@implementation EXTExpenseDetailsEditAssembly

- (EXTExpenseDetailsEditViewController *)viewExpenseDetailsEdit {
    return [TyphoonDefinition withClass:[EXTExpenseDetailsEditViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterExpenseDetailsEdit]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterExpenseDetailsEdit]];
                              [definition injectProperty:@selector(keyboardControl)
                                                    with:[self.keyboardAssembly keyboardControl]];
                          }];
}

- (EXTExpenseDetailsEditInteractor *)interactorExpenseDetailsEdit {
    return [TyphoonDefinition withClass:[EXTExpenseDetailsEditInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterExpenseDetailsEdit]];
                          }];
}

- (EXTExpenseDetailsEditPresenter *)presenterExpenseDetailsEdit{
    return [TyphoonDefinition withClass:[EXTExpenseDetailsEditPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewExpenseDetailsEdit]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorExpenseDetailsEdit]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerExpenseDetailsEdit]];
                              [definition injectProperty:@selector(expenseService) with:[self.globalAssembly expenseService]];
                          }];
}

- (EXTExpenseDetailsEditRouter *)routerExpenseDetailsEdit{
    return [TyphoonDefinition withClass:[EXTExpenseDetailsEditRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewExpenseDetailsEdit]];
                              [definition injectProperty:@selector(presenter) with:[self presenterExpenseDetailsEdit]];
                          }];
}





- (EXTExpenseDetailsEditViewController *)viewExpenseDetailsCreate {
    return [TyphoonDefinition withClass:[EXTExpenseDetailsEditViewController class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterExpenseDetailsCreate]];
                              [definition injectProperty:@selector(moduleInput)
                                                    with:[self presenterExpenseDetailsEdit]];
                              [definition injectProperty:@selector(keyboardControl)
                                                    with:[self.keyboardAssembly keyboardControl]];
                          }];
}

- (EXTExpenseDetailsEditInteractor *)interactorExpenseDetailsCreate {
    return [TyphoonDefinition withClass:[EXTExpenseDetailsCreateInteractor class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(output)
                                                    with:[self presenterExpenseDetailsCreate]];
                          }];
}

- (EXTExpenseDetailsEditPresenter *)presenterExpenseDetailsCreate{
    return [TyphoonDefinition withClass:[EXTExpenseDetailsEditPresenter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(view)
                                                    with:[self viewExpenseDetailsCreate]];
                              [definition injectProperty:@selector(interactor)
                                                    with:[self interactorExpenseDetailsCreate]];
                              [definition injectProperty:@selector(router)
                                                    with:[self routerExpenseDetailsCreate]];
                              [definition injectProperty:@selector(expenseService) with:[self.globalAssembly expenseService]];
                          }];
}
    
- (EXTExpenseDetailsEditRouter *)routerExpenseDetailsCreate{
    return [TyphoonDefinition withClass:[EXTExpenseDetailsEditRouter class]
                          configuration:^(TyphoonDefinition *definition) {
                              [definition injectProperty:@selector(transitionHandler)
                                                    with:[self viewExpenseDetailsEdit]];
                              [definition injectProperty:@selector(presenter) with:[self presenterExpenseDetailsCreate]];
                          }];
}



@end
