//
//  EXTExpenseDetailsEditInteractorInput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXTExpenseNavigationItemProtocol.h"
#import "EXTExpenseServiceProtocol.h"
#import "TitleProtocol.h"

@protocol EXTExpenseDetailsEditInteractorInput <EXTExpenseNavigationItemProtocol, TitleProtocol>

@required
@property(nonatomic, strong) id<EXTExpenseServiceProtocol> expenseService;

@end
