//
//  EXTExpenseDetailsCreateInteractor.m
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 15.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTExpenseDetailsCreateInteractor.h"

@implementation EXTExpenseDetailsCreateInteractor

#pragma mark - Методы EXTExpenseDetailsEditInteractorInput
@synthesize expenseService = _expenseService;

- (NSString *)itemTitle {
    return @"Создать";
}

- (void)onRightBarButtonItemTap {
    NSLog(@";lkj;lkj");
}

- (NSString *)title {
    //Здесь нужно добавить локализацию
    return @"Новая операция";
}

@end
