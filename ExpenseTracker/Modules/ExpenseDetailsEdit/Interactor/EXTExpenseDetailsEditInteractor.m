//
//  EXTExpenseDetailsEditInteractor.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpenseDetailsEditInteractor.h"

#import "EXTExpenseDetailsEditInteractorOutput.h"
#import "EXTExpenseProtocol.h"

@implementation EXTExpenseDetailsEditInteractor

#pragma mark - Методы EXTExpenseDetailsEditInteractorInput
@synthesize expenseService = _expenseService;

- (NSString *)itemTitle {
    return @"Готово";
}

- (void)onRightBarButtonItemTap {
    NSLog(@"asd");
}

- (NSString *)title {
    return self.output.expense.category.name;
}

@end
