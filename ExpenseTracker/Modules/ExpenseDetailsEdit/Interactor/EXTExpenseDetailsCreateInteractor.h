//
//  EXTExpenseDetailsCreateInteractor.h
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 15.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTExpenseDetailsEditInteractorInput.h"

@protocol EXTExpenseDetailsEditInteractorOutput;

@interface EXTExpenseDetailsCreateInteractor : NSObject <EXTExpenseDetailsEditInteractorInput>

@property (nonatomic, weak) id<EXTExpenseDetailsEditInteractorOutput> output;

@end
