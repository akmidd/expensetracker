//
//  EXTExpenseDetailsEditInteractor.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import "EXTExpenseDetailsEditInteractorInput.h"

@protocol EXTExpenseDetailsEditInteractorOutput;

@interface EXTExpenseDetailsEditInteractor : NSObject <EXTExpenseDetailsEditInteractorInput>

@property (nonatomic, weak) id<EXTExpenseDetailsEditInteractorOutput> output;

@end
