//
//  EXTExpenseDetailsEditInteractorOutput.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 SberTech. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EXTExpenseProtocol;

@protocol EXTExpenseDetailsEditInteractorOutput <NSObject>

@property (nonatomic, strong, readonly) id<EXTExpenseProtocol> expense;

@end
