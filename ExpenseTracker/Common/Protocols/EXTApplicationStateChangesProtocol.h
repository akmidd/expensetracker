//
//  EXTApplicationStateChangesProtocol.h
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 16.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EXTBaseAppDelegateMethodsProtocol.h"

@protocol EXTApplicationStateChangesProtocol <EXTBaseAppDelegateMethodsProtocol>

@end
