//
//  EXTBaseAppDelegateMethodsProtocol.h
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 16.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EXTBaseAppDelegateMethodsProtocol <NSObject>

@optional
- (void)applicationWillResignActive:(UIApplication *)application;
- (void)applicationDidEnterBackground:(UIApplication *)application;
- (void)applicationWillEnterForeground:(UIApplication *)application;
- (void)applicationDidBecomeActive:(UIApplication *)application;
- (void)applicationWillTerminate:(UIApplication *)application;

@end
