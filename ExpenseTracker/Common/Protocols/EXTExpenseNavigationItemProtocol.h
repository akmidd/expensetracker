//
//  EXTExpenseNavigationItemProtocol.h
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 15.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EXTExpenseNavigationItemProtocol <NSObject>

@required
- (NSString*)itemTitle;
- (void)onRightBarButtonItemTap;

@end
