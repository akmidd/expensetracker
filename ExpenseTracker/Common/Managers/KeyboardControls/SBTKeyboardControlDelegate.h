//
//  SBTKeyboardControlDelegate.h
//  SBTCrowdSourcing
//
//  Created by Pavel Nefedov on 28.08.17.
//  Copyright © 2017 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SBTKeyboardControlDelegate <NSObject>

@required
- (void)keyboardSubmitButtonTapped;

@end
