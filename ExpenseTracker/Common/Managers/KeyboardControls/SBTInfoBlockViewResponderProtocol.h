//
//  SBTInfoBlockViewResponderProtocol.h
//  SBTCrowdSourcing
//
//  Created by Maxim on 31/08/2017.
//  Copyright © 2017 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SBTInfoBlockViewResponderProtocol <NSObject>

@required
@property (nonatomic, weak, readonly) UIResponder *responder;

@end
