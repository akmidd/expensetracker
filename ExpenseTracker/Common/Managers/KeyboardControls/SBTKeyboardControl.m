//
//  SBTKeyboardControl.m
//  SBTCrowdSourcing
//
//  Created by Pavel Nefedov on 28.08.17.
//  Copyright © 2017 Sberbank. All rights reserved.
//

#import "SBTKeyboardControl.h"
#import "SBTInfoBlockViewResponderProtocol.h"

static CGFloat kToolBarHeight = 45.f;

@interface SBTKeyboardControl ()

@property (nonatomic, weak) UIResponder *currentInput;

@property (nonatomic, strong) UIBarButtonItem *flexSpace;
@property (nonatomic, strong) UIBarButtonItem *flexSpaceCenter;
@property (nonatomic, strong) UIBarButtonItem *flexSpaceRight;

@property (nonatomic, strong) UIBarButtonItem *nextButton;
@property (nonatomic, strong) UIBarButtonItem *submitButton;
@property (nonatomic, strong) UIButton *doneButton;
@property (nonatomic, strong) UIBarButtonItem *hideKeyboardButton;
@property (nonatomic, strong) UIBarButtonItem *hideKeyboardButton2;
@property (nonatomic, strong) UIBarButtonItem *hideKeyboardButton3;

@property (nonatomic, strong) UIToolbar *inputSubmitAccessoryView;
@property (nonatomic, strong) UIToolbar *inputNextAccessoryView;
@property (nonatomic, strong) UIToolbar *inputHideAccessoryView;

@property (nonatomic, weak) UIScrollView *scrollView;

@end


@implementation SBTKeyboardControl
@synthesize inputFields = _inputFields;
@synthesize delegate = _delegate;

#pragma mark - initialization

- (instancetype)initWithInputFields:(NSArray *)inputFields delegate:(id<SBTKeyboardControlDelegate>)delegate scrollView:(UIScrollView *)scrollView{
    self = [super init];
    if (self) {
        _scrollView = scrollView;
        _delegate = delegate;
        [self setInputFields:inputFields];
        [self sbt_addKeyboardNotofocations];
    }
    return self;
}

- (void)updateWithInputFields:(NSArray *)inputFields delegate:(id<SBTKeyboardControlDelegate>)delegate scrollView:(UIScrollView *)scrollView {
    _scrollView = scrollView;
    _delegate = delegate;
    [self setInputFields:inputFields];
    [self sbt_addKeyboardNotofocations];
}

- (void)sbt_addKeyboardNotofocations {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

#pragma mark -
#pragma mark Notification methods

- (void)keyboardWillHide:(NSNotification *)notification{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)keyboardWillShow:(NSNotification *)notification{
    NSDictionary* info = [notification userInfo];
    float keyboardHeight = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height + 1;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardHeight, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIBarButtonItem *)nextButton {
    if (!_nextButton) {
        UIButton *nextButtonView = [UIButton buttonWithType:UIButtonTypeCustom];
        nextButtonView.frame = CGRectMake(0, 0, 56, 20);
        [nextButtonView.titleLabel setFont:[UIFont systemFontOfSize:13.]];
        [nextButtonView setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [nextButtonView setTitle:NSLocalizedString(@"Далее", nil) forState:UIControlStateNormal];
        [nextButtonView addTarget:self action:@selector(focusNext:) forControlEvents:UIControlEventTouchUpInside];
        _nextButton = [[UIBarButtonItem alloc] initWithCustomView:nextButtonView];
    }
    return _nextButton;
}

- (UIBarButtonItem *)submitButton {
    if (!_submitButton) {
        _doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _doneButton.frame = CGRectMake(0, 0, 100, 20);
        [_doneButton.titleLabel setFont:[UIFont systemFontOfSize:13.]];
        [_doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_doneButton setTitle:NSLocalizedString(@"Продолжить", nil) forState:UIControlStateNormal];
        [_doneButton addTarget:self action:@selector(submitForm:) forControlEvents:UIControlEventTouchUpInside];
        _submitButton = [[UIBarButtonItem alloc] initWithCustomView:_doneButton];
//        _doneButton.enabled = NO;
        _doneButton.hidden = YES;
    }
    return _submitButton;
}

- (UIBarButtonItem *)hideKeyboardButton {
    if (!_hideKeyboardButton) {
        UIButton *hideKeyboardButtonView = [self closeButton];
        _hideKeyboardButton = [[UIBarButtonItem alloc] initWithCustomView:hideKeyboardButtonView];
    }
    return _hideKeyboardButton;
}

- (UIBarButtonItem *)hideKeyboardButton2 {
    if (!_hideKeyboardButton2) {
        UIButton *hideKeyboardButtonView = [self closeButton];
        _hideKeyboardButton2 = [[UIBarButtonItem alloc] initWithCustomView:hideKeyboardButtonView];
    }
    return _hideKeyboardButton2;
}

- (UIBarButtonItem *)hideKeyboardButton3 {
    if (!_hideKeyboardButton3) {
        UIButton *hideKeyboardButtonView = [self closeButton];
        _hideKeyboardButton3 = [[UIBarButtonItem alloc] initWithCustomView:hideKeyboardButtonView];
    }
    return _hideKeyboardButton3;
}

- (UIButton*)closeButton {
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(0, 0, 66, 20);
    [closeButton.titleLabel setFont:[UIFont systemFontOfSize:13.]];
    [closeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [closeButton setTitle:NSLocalizedString(@"Закрыть", nil) forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(hideInputView:) forControlEvents:UIControlEventTouchUpInside];
    return closeButton;
}

- (UIBarButtonItem *)flexSpace {
    if (!_flexSpace) {
        _flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                   target:nil
                                                                   action:nil];
    }
    return _flexSpace;
}

- (UIBarButtonItem *)flexSpaceCenter {
    if (!_flexSpaceCenter) {
        _flexSpaceCenter = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                        target:nil
                                                                        action:nil];
    }
    return _flexSpaceCenter;
}

- (UIBarButtonItem *)flexSpaceRight {
    if (!_flexSpaceRight) {
        _flexSpaceRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil
                                                                         action:nil];
    }
    return _flexSpaceRight;
}

- (UIToolbar *)inputSubmitAccessoryView {
    if (!_inputSubmitAccessoryView) {
        CGRect appFrame = [[UIScreen mainScreen] bounds];
        _inputSubmitAccessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0., 0., CGRectGetWidth(appFrame), kToolBarHeight)];
        _inputSubmitAccessoryView.items = @[self.hideKeyboardButton2 ,self.flexSpace, self.submitButton];
    }
    return _inputSubmitAccessoryView;
}

- (UIToolbar *)inputNextAccessoryView {
    if (!_inputNextAccessoryView) {
        CGRect appFrame = [[UIScreen mainScreen] bounds];
        _inputNextAccessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0., 0., CGRectGetWidth(appFrame), kToolBarHeight)];
        _inputNextAccessoryView.items = @[self.hideKeyboardButton, self.flexSpaceCenter, self.nextButton];
    }
    return _inputNextAccessoryView;
}

- (UIToolbar *)inputHideAccessoryView {
    if (!_inputHideAccessoryView) {
        CGRect appFrame = [[UIScreen mainScreen] bounds];
        _inputHideAccessoryView = [[UIToolbar alloc] initWithFrame:CGRectMake(0., 0., CGRectGetWidth(appFrame), kToolBarHeight)];
        _inputHideAccessoryView.items = @[self.hideKeyboardButton3, self.flexSpaceRight];
    }
    return _inputHideAccessoryView;
}

- (void)setInputFields:(NSArray *)inputFields {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    _inputFields = [self filteredInputFields:inputFields];
    id<SBTInfoBlockViewResponderProtocol> last;
    for (id<SBTInfoBlockViewResponderProtocol> input in _inputFields) {
        [self setupForView:input inputAccessoryView:self.inputNextAccessoryView];
        last = input;
    }
    
    [self setupForView:last inputAccessoryView:self.inputSubmitAccessoryView];
//    [self setupForView:last inputAccessoryView:self.inputHideAccessoryView];
}

- (void)setupForView:(id<SBTInfoBlockViewResponderProtocol>)input inputAccessoryView:(UIToolbar *)toolbar{
    if ([input.responder respondsToSelector:@selector(setInputAccessoryView:)]) {
        [input.responder performSelector:@selector(setInputAccessoryView:) withObject:toolbar];
    }
}

- (NSArray<id<SBTInfoBlockViewResponderProtocol>> *)filteredInputFields:(NSArray *)inputFields {
    NSMutableArray *textFieldInputViews = [NSMutableArray new];
    for (id inputView in inputFields) {
        if ([inputView conformsToProtocol:@protocol(SBTInfoBlockViewResponderProtocol)]) {
            [textFieldInputViews addObject:inputView];
        }
    }
    return textFieldInputViews;
}

#pragma mark - actions & notification handlers

- (void) setSubmitButtonEnabled:(BOOL)enabled {
    self.doneButton.enabled = enabled;
    if(self.doneButton.enabled){
        [self.doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    else{
        [self.doneButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    }
}

- (void)setSubmitButtonHidden:(BOOL)hidden {
    self.doneButton.hidden = hidden;
}

- (void) setSubmitButtonTitle:(NSString *)title {
    [self.doneButton setTitle:title forState:UIControlStateNormal];
}

- (void)inputDidBeginEditing:(NSNotification *)notification {
}

- (NSUInteger)currentInputIndex {
    return [self.inputFields indexOfObjectPassingTest:^BOOL(id<SBTInfoBlockViewResponderProtocol> obj, NSUInteger idx, BOOL *stop) {
        return [[obj responder] isFirstResponder];
    }];
}

- (UIResponder *)currentInput {
    if (![_currentInput isFirstResponder]) {
        NSUInteger index = [self currentInputIndex];
        _currentInput = index != NSNotFound ? self.inputFields[index] : nil;
    }
    return _currentInput;
}

- (NSUInteger)nextVisibleControlIndex:(NSUInteger)index {
    if (index != NSNotFound) {
        while (index < [self.inputFields count] - 1) {
            index++;
            UIControl *control = self.inputFields[index];
            if (!control.hidden) {
                return index;
            }
        }
    }
    return NSNotFound;
}

- (void)focusNext:(id)sender {
    NSUInteger index = [self nextVisibleControlIndex:[self currentInputIndex]];
    if (index != NSNotFound) {
        [[self.inputFields[index] responder] becomeFirstResponder];
    }
}

- (void)hideInputView:(id)sender {
    NSUInteger index = [self currentInputIndex];
    if (index != NSNotFound) {
        [[self.inputFields[index] responder] resignFirstResponder];
    }
}

- (void)submitForm:(id)sender {
    if ([_delegate respondsToSelector:@selector(keyboardSubmitButtonTapped)]) {
        [_delegate keyboardSubmitButtonTapped];
    }
    [self closeInput:nil];
}

- (void)closeInput:(id)sender {
    [self.currentInput resignFirstResponder];
}

@end
