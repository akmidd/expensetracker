//
//  SBTKeyboardControl.h
//  SBTCrowdSourcing
//
//  Created by Pavel Nefedov on 28.08.17.
//  Copyright © 2017 Sberbank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBTKeyboardControlProtocol.h"
#import "SBTKeyboardControlDelegate.h"

@interface SBTKeyboardControl : NSObject <SBTKeyboardControlProtocol>

@end
