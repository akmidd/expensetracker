//
//  SBTKeyboardControlProtocol.h
//  SBTCrowdSourcing
//
//  Created by Pavel Nefedov on 28.08.17.
//  Copyright © 2017 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBTKeyboardControlDelegate.h"

@protocol SBTInfoBlockViewProtocol;

@protocol SBTKeyboardControlProtocol <NSObject>

@required
@property (nonatomic, strong) NSArray* inputFields;
@property (nonatomic, weak) id<SBTKeyboardControlDelegate> delegate;

- (instancetype)initWithInputFields:(NSArray *)inputFields delegate:(id<SBTKeyboardControlDelegate>) delegate scrollView:(UIScrollView*)scrollView;
- (void)updateWithInputFields:(NSArray *)inputFields delegate:(id<SBTKeyboardControlDelegate>)delegate scrollView:(UIScrollView *)scrollView;
- (void)setSubmitButtonEnabled:(BOOL)enabled;
- (void)setSubmitButtonHidden:(BOOL)hidden;
- (void)setSubmitButtonTitle:(NSString *)title;
- (void)focusNext:(id)sender;

@end
