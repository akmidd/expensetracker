//
//  EXTApplicationStateTrackerManager.h
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 16.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EXTApplicationStateTrackerManagerProtocol.h"

@interface EXTApplicationStateTrackerManager : NSObject <EXTApplicationStateTrackerManagerProtocol>

@end
