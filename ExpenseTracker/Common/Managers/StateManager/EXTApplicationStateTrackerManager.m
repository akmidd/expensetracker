//
//  EXTApplicationStateTrackerManager.m
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 16.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTApplicationStateTrackerManager.h"

@interface EXTApplicationStateTrackerManager()

@property (nonatomic, strong) NSMutableSet *delegates;

@end

@implementation EXTApplicationStateTrackerManager

- (instancetype)init {
    self = [super init];
    if (self) {
        _delegates = [NSMutableSet set];
    }
    return self;
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    @synchronized (self) {
        NSArray *array = [self.delegates allObjects]; //Эта строка кода обязательна, чтобы не было проблем с потоками
        for (id<EXTApplicationStateChangesProtocol> delegate in array) {
            if ([delegate respondsToSelector:@selector(applicationWillEnterForeground:)]) {
                [delegate applicationWillEnterForeground:application];
            }
        }
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    @synchronized (self) {
        NSArray *array = [self.delegates allObjects]; //Эта строка кода обязательна, чтобы не было проблем с потоками
        for (id<EXTApplicationStateChangesProtocol> delegate in array) {
            if ([delegate respondsToSelector:@selector(applicationDidEnterBackground:)]) {
                [delegate applicationDidEnterBackground:application];
            }
        }
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {
    @synchronized (self) {
        NSArray *array = [self.delegates allObjects]; //Эта строка кода обязательна, чтобы не было проблем с потоками
        for (id<EXTApplicationStateChangesProtocol> delegate in array) {
            if ([delegate respondsToSelector:@selector(applicationWillResignActive:)]) {
                [delegate applicationWillResignActive:application];
            }
        }
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    @synchronized (self) {
        NSArray *array = [self.delegates allObjects]; //Эта строка кода обязательна, чтобы не было проблем с потоками
        for (id<EXTApplicationStateChangesProtocol> delegate in array) {
            if ([delegate respondsToSelector:@selector(applicationDidBecomeActive:)]) {
                [delegate applicationDidBecomeActive:application];
            }
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [self reset];
}

- (void)addTracker:(id<EXTApplicationStateChangesProtocol>)tracker {
    if (![tracker conformsToProtocol:@protocol(EXTApplicationStateChangesProtocol)]) {
        return;
    }
    
    __weak typeof(tracker) weakTracker = tracker;
    [self.delegates addObject:weakTracker];
}

- (void)dealloc {
    [self reset];
}

- (void)reset {
    [self.delegates removeAllObjects];
}

@end
