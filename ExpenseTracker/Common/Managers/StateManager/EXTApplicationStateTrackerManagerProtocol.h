//
//  EXTApplicationStateTrackerManagerProtocol.h
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 16.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EXTBaseAppDelegateMethodsProtocol.h"
#import "EXTApplicationStateChangesProtocol.h"

@protocol EXTApplicationStateTrackerManagerProtocol <EXTBaseAppDelegateMethodsProtocol>

@required
- (void)addTracker:(id<EXTApplicationStateChangesProtocol>)tracker;

@end
