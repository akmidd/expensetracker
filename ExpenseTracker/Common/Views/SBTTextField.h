//
//  SBTTextField.h
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 22.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBTInfoBlockViewResponderProtocol.h"

@interface SBTTextField : UITextField <SBTInfoBlockViewResponderProtocol>

@end
