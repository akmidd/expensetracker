//
//  UIViewController+TapToHideKeyboard.m
//  SBTCrowdSourcing
//
//  Created by Pavel Nefedov on 25.08.17.
//  Copyright © 2017 Sberbank. All rights reserved.
//

#import "UIViewController+TapToHideKeyboard.h"

#import <objc/runtime.h>

@interface UIView ()

@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@end

@implementation UIView (TapToHideKeyboard)

- (void)setHidesKeyboardOnTap:(BOOL)hidesKeyboardOnTap {
    objc_setAssociatedObject(self, @selector(setHidesKeyboardOnTap:), @(hidesKeyboardOnTap), OBJC_ASSOCIATION_RETAIN);
    
    if (hidesKeyboardOnTap) {
        if (self.tapGesture) {
            return;
        }
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap)];
        tap.cancelsTouchesInView = NO;
        [self addGestureRecognizer:tap];
    } else {
        if (self.tapGesture) {
            [self removeGestureRecognizer:self.tapGesture];
            self.tapGesture = nil;
        }
    }
}
- (BOOL)hidesKeyboardOnTap {
    return [objc_getAssociatedObject(self, @selector(setHidesKeyboardOnTap:)) boolValue];
}

- (void)setTapGesture:(UITapGestureRecognizer *)tapGesture {
    objc_setAssociatedObject(self, @selector(setTapGesture:), tapGesture, OBJC_ASSOCIATION_RETAIN);
}
- (UITapGestureRecognizer *)tapGesture {
    return objc_getAssociatedObject(self, @selector(setTapGesture:));
}

- (void)triggerHideKeyboard {
    [self handleTap];
}

#pragma mark - Handlers

- (void)handleTap {
    sbt_hideKeyboard();
}

void sbt_hideKeyboard() {
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}


@end
