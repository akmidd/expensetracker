//
//  UIViewController+TapToHideKeyboard.h
//  SBTCrowdSourcing
//
//  Created by Pavel Nefedov on 25.08.17.
//  Copyright © 2017 Sberbank. All rights reserved.
//

#import <UIKit/UIKit.h>

void sbt_hideKeyboard();

@interface UIView (TapToHideKeyboard)

@property (nonatomic, assign) BOOL hidesKeyboardOnTap;
- (void)triggerHideKeyboard;

@end
