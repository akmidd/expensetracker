//
//  EXTAppStateAssembly.h
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 16.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "TyphoonAssembly.h"
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "EXTApplicationStateTrackerManagerProtocol.h"

@interface EXTAppStateAssembly : TyphoonAssembly <RamblerInitialAssembly>

-(id<EXTApplicationStateTrackerManagerProtocol>) appStateTrackingManager;

@end
