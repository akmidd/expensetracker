//
//  EXTAppStateAssembly.m
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 16.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTAppStateAssembly.h"
#import "EXTApplicationStateTrackerManager.h"

@implementation EXTAppStateAssembly

-(id<EXTApplicationStateTrackerManagerProtocol>) appStateTrackingManager {
    return [TyphoonDefinition withClass:[EXTApplicationStateTrackerManager class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

@end
