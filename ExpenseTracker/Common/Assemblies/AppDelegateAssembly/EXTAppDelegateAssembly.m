//
//  EXTAppDelegateAssembly.m
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 16.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTAppDelegateAssembly.h"
#import "AppDelegate.h"
#import "EXTAppStateAssembly.h"

@interface EXTAppDelegateAssembly()

@property (nonatomic, weak) EXTAppStateAssembly *appStateAssembly;

@end

@implementation EXTAppDelegateAssembly

-(AppDelegate*) appDelegateKey {
    return [TyphoonDefinition withClass:[AppDelegate class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
        [definition injectProperty:@selector(trackingManager)
                              with:[self.appStateAssembly appStateTrackingManager]];
    }];
}

@end
