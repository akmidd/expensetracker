//
//  EXTAppDelegateAssembly.h
//  ExpenseTracker
//
//  Created by Pavel Nefedov on 16.02.2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "TyphoonAssembly.h"
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@interface EXTAppDelegateAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
