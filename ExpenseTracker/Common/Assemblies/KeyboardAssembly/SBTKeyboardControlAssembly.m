//
//  SBTKeyboardControlAssembly.m
//  SBTCrowdSourcing
//
//  Created by Maxim on 30/08/2017.
//  Copyright © 2017 Sberbank. All rights reserved.
//

#import "SBTKeyboardControlAssembly.h"
#import "SBTKeyboardControl.h"

@implementation SBTKeyboardControlAssembly

- (id<SBTKeyboardControlProtocol>)keyboardControl {
    return [TyphoonDefinition withClass:[SBTKeyboardControl class]];
}

@end
