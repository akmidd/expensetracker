//
//  SBTKeyboardControlAssembly.h
//  SBTCrowdSourcing
//
//  Created by Maxim on 30/08/2017.
//  Copyright © 2017 Sberbank. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "SBTKeyboardControlProtocol.h"

@interface SBTKeyboardControlAssembly : TyphoonAssembly <RamblerInitialAssembly>

- (id<SBTKeyboardControlProtocol>)keyboardControl;

@end
