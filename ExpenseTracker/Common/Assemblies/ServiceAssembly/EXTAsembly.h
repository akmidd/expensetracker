//
//  EXTAsembly.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "EXTExpenseServiceProtocol.h"

@interface EXTAsembly : TyphoonAssembly <RamblerInitialAssembly>

-(id<EXTExpenseServiceProtocol>) expenseService;

@end
