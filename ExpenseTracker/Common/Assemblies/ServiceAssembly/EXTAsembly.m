//
//  EXTAsembly.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 08/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTAsembly.h"
#import "EXTExpenseService.h"

@implementation EXTAsembly

-(id<EXTExpenseServiceProtocol>) expenseService {
    return [TyphoonDefinition withClass:[EXTExpenseService class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

@end
