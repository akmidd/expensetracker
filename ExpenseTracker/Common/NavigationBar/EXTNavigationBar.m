//
//  EXTNavigationBar.m
//  ExpenseTracker
//
//  Created by Maxim on 19/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "EXTNavigationBar.h"

@implementation EXTNavigationBar

+ (void)initialize {
    if (self == [EXTNavigationBar self]) {
        [EXTNavigationBar appearance].translucent = NO;
        [EXTNavigationBar appearance].tintColor = [UIColor whiteColor];
        [EXTNavigationBar appearance].barTintColor = [UIColor colorWithRed:66.0f/255.0f green:196.0f/255.0f blue:110.0f/255.0f alpha:1.0f];
        [EXTNavigationBar appearance].titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
        if (@available(iOS 11.0, *)) {
            [EXTNavigationBar appearance].largeTitleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
        }
    }
}
@end
