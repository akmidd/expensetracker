//
//  AppDelegate+AppDelegate_Siri.h
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 15/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Siri)
-(void) requestSiriAuthorization;
-(void) setupVocabulary;
@end
