//
//  AppDelegate+AppDelegate_Siri.m
//  ExpenseTracker
//
//  Created by sbt-klochkov-dv on 15/02/2018.
//  Copyright © 2018 Sberbank. All rights reserved.
//

#import "AppDelegate+Siri.h"
#import <Intents/Intents.h>

@implementation AppDelegate (AppDelegate_Siri)

-(void) requestSiriAuthorization {
    [INPreferences requestSiriAuthorization:^(INSiriAuthorizationStatus status) {
        if(status == INSiriAuthorizationStatusAuthorized) {
            NSLog(@"INSiriAuthorizationStatusAuthorized");
        } else {
            NSLog(@"INSiriAuthorizationStatusDenied");
        }
    }];
}

-(void) setupVocabulary {
    
    NSOrderedSet * contactNames = [NSOrderedSet orderedSetWithArray:@[@"Продукты"]];
    
    [INVocabulary.sharedVocabulary setVocabularyStrings:contactNames ofType:(INVocabularyStringTypeContactName)];
}

@end
